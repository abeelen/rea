"""
NAM: TestMap.py
DES: test map plotting
HIS: FB040608 modified Frederic's original version
     FB040611 add small mambo map
     FS050502 adapted to new Boa structure
USE:

open('/xserve')
execfile('TestMap.py')

"""
data.MessHand.setMaxWeight(5)
# Open 2 XW devices
op('/xw')
op('/xw')

# set(ciWedge,2)

indir('../Fits')
infile("822")       # 16 sub map
read()
chan(range(17))

# Plot the chanmap in device 1
device(1)
chanmap()

# chanmap(fit=0)
print "for SIMBA map  - press <Enter>"
raw_input()

# infile('APEX-856')
infile('APEX-861a')
# infile('APEX-883')  # looks strange
# infile('APEX-891')
# infile('APEX-805')
data.MessHand.setMaxWeight(5)

read()
chan([1])

# Plot the chanmap in device 1
device(1)
chanmap()

print "Signals  - press <Enter>"
raw_input()

# limits(['*','*',-6.e5,8.e5])
channel([1])
# plot the signal in device 2
device(2)
signal(ci=1)

print "0-order baseline subtracted - press <Enter>"
raw_input()

# BoaB.currData.Array_Geo[0,:] = [ -9.4, 141.5, 92.5, 41.5, -60.4 ]
readR('SIMBA.rcp')
flagLon(above=-60, below=60, flag=10)
stat()
basesub(channel=-1, phase=-1, order=2, plot=0, subtract=1)
unflag(flag=10)

# plot the signal in device 2
device(2)
signal(ci=2, overplot=1)

print "New Channel map  - press <Enter>"
raw_input()

# plot the chanmap in device 2
device(2)
chanmap()

print "all channel maps  - press <Enter>"
raw_input()

chan(range(6))
# plot all the chanmap in device 1
device(1)
chanmap()

print "smooth  - press <Enter>"
raw_input()

# plot smoothed chanmap in device 2
device(2)
chanmap(oversamp=1.)
