# OTF map on Orion-IRC2
read('59491')
data.medianBaseline()
plotRmsChan()    # check where most channels are
flagRms(above=1)
flagRms(below=0.2)
# read (so far) best RCP
data.BolometerArray.updateRCP('jup-44830-32-improved.rcp')
# Flag source
data.flagPosition(radius=150.)
base(order=1)
data.medianNoiseRemoval()
plotRmsChan()    # check new distribution
flagRms(above=0.5)
sig()            # see how signal looks like
flagC([140, 227])  # bad channels
despike()
# compute weights
data.computeWeight()
# unflag source
unflag(flag=5)
# quick map in HO system
mapping()
raw_input()

# nicer map in EQ system
data.doMap(system='EQ', sizeX=[83.9, 83.73], sizeY=[-5.48, -5.28], oversamp=5.)
data.Map.smoothBy(6. / 3600.)
data.Map.display(caption=data.ScanParam.caption())
