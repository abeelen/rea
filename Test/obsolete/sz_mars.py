BoaConfig.projID = 'X-00.F-0001-2005'
data.MessHand.setMaxWeight(5)
data.read('184')

data.flagChannels([23, 35, 85, 96, 93, 113, 129, 130, 133, 134, 135, 149])
data.flagChannels(
    [153,
     154,
     155,
     157,
     169,
     170,
     171,
     172,
     173,
     174,
     175,
     176])
data.flagChannels([65, 128, 131, 148, 150, 152, 167])
data.findSubscan()

# data.flag('flux',above=7700)
# data.flag('flux',below=-7700)

data.basePolySubscan(order=3)
# data.flagLon(below=-1200.)
# data.flagLon(above=1300.)

op('clean_map.gif/gif')
data.fastMap(sizeX=[-1200, 1400], sizeY=[-1800, 1600], style='gray')
close()
