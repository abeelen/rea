"""
NAM: TestSNF.py
DES: test skynoise filter on simba-APEX data
     [colors: 1=w  2=red 3=green 4=blue 5=l-blue 6=pink 7=yellow]
HIS: FB040602
     FS040922: use new 'plotcor' command
     FS050321  use new Bogli syntax

USE: execfile('TestSNF.py')

"""
from Numeric import *
from boa import BoaPointing

data = BoaPointing.Point()

data.MessHand.setMaxWeight(5)      # set message weight to max verbosity

indir("../fits/")
# data.read("APEX-420")  # strong pointing
data.read("APEX-395")   # weak pointing (snf will fail on that!)
# data.read("APEX-805")   # Jupiter map

op('/xw')
data.fastMap()
data.MessHand.info("First Map... press <Enter>")
raw_input()

refChan = 1    # select reference channel

data.statistics()  # compute statistics
data.basePoly(
    chanList='all',
     order=0,
     plot=0,
     subtract=1)  # fit 0-order baseline
data.statistics()  # recompute statistics

# fit 1-order baseline to each subscan separately
data.basePolySubscan(chanList='all', order=1, plot=0, subtract=1)

data.statistics()  # recompute statistics
Rms = data.ChanRms[refChan]

data.signal(ci=1, style='l', limitsY=[-4 * Rms, 4 * Rms])
data.signal(style='p', overplot=1)

data.despike(below=-3., above=3.)           # flag all data outside +-x rms
data.signal(ci=2, style='l', overplot=1)

#-------------------------------------------------------------------------
data.computeCorMatrix()    # compute correlation matrix
print "To plot correlation ... press <Enter>"
raw_input()

data.correlate(refChan, plot=1)

# compute skynoise for all channels
for chan in data.BolometerArray.CurrChanList:
    data.computeSkyNoise(channel=chan)

print "To correlate SN ... press <Enter>"
raw_input()

# Correlate Skynoise of RefChan with signal of other channels
data.correlate(refChan, plot=1, skynoise=1)


#-------------------------------------------------------------------------

print "To plot SN subtracted signal ... press <Enter>"
raw_input()
data.signal(ci=3, style='l', limitsY=[-2 * Rms, 2.5 * Rms])

# recompute skynoise for all channels and remove it
for chan in data.BolometerArray.CurrChanList:
    data.computeSkyNoise(channel=chan, subtract=1)

data.signal(ci=4, style='l', limitsY=[-2 * Rms, 2.5 * Rms], overplot=1)


#-------------------------------------------------------------------------

print "Now blanking sources and fitting baseline ... press <Enter>"
raw_input()

data.DataBac = array(data.Data)  # data backup
data.flagLST(above=55620, below=55650, flag=10)
data.flagLST(above=55780, below=55830, flag=10)
print "basesub"
data.basePolySubscan(chanList=[1], order=6, plot=1, subtract=0)
data.unflag(flag=10)

for i_ch in [2, 3, 4, 5]:
    data.basePolySubscan(chanList=[i_ch], order=4, plot=1, subtract=0)

data.signal(ci=7, overplot=1)
data.Data = array(data.DataBac)   # recover Data

data.flagLST(above=55620, below=55650, flag=10)
data.flagLST(above=55780, below=55830, flag=10)
data.basePolySubscan(chanList=[1], order=6, plot=0, subtract=1)
data.unflag(flag=10)

for i_ch in [2, 3, 4, 5]:
    data.basePolySubscan(chanList=[i_ch], order=4, plot=0, subtract=1)

data.signal(ci=2, overplot=1)

#-------------------------------------------------------------------------
print "For pointing analysis ... press <Enter>"
raw_input()

op()
data.fastMap(oversamp=5.)
data.solvePointing(radius=120., plot=1)


# flagC([2])
