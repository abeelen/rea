# Automatically adapted for numpy.oldnumeric Nov 22, 2012 by

"""
NAM: test.bogli.py
DES: test bogli plotting functions from python
     no need to use boa
USE: python
     execfile('Test/test.bogli.py')
"""
# HIS: ???      initial version
#     FB051120 add header and stops

import numpy.oldnumeric as Numeric
import numpy.oldnumeric.random_array as RandomArray
import time
from .Bogli import DeviceHandler, Plot, MultiPlot

DeviceHandler.openDev('/XWINDOW')

print 'you may resize the window as you like, return to continue'
raw_input()
DeviceHandler.resizeDev()

x = Numeric.array(range(100), Numeric.Float) / 10

Plot.plot(x, Numeric.sqrt(x), limitsX=[1, 5])
# Note that Y limits are then computed according to this X range

print 'return to continue, Ctrl C to stop'
raw_input()

Plot.plot(x, x * x, labelX='blah', labelY='blah2', caption='capatation')
# Note that plot clear the screen first, you need to use the new
# 'overplot' keyword (see below)

print 'return to continue, Ctrl C to stop'
raw_input()

Plot.plot(x, x * x * x, overplot=1, ci=2, style='l')
# You can change
# - the plot style ('l': line, 'p': point, 'b':bins)
# - the plot color ci=1,2,3

print 'return to continue, Ctrl C to stop'
raw_input()

n_point = 365
chanlist = range(n_point)

x2 = RandomArray.random([n_point, n_point])
y2 = RandomArray.random([n_point, n_point])

MultiPlot.plot(chanlist, x2, y2 + x2, style='p')

print 'return to continue, Ctrl C to stop'
raw_input()

# Can you see the correlation ?
for i in range(x2.shape[0]):
    # Sorting to use a dirty way of plotting lines
    # Lines should be 2 points... but...
    x2[i] = Numeric.sort(x2[i])

MultiPlot.plot(chanlist, x2, x2 + 0.5, style='l', ci=2, overplot=1)

mapping = Numeric.absolute(RandomArray.standard_normal([n_point, n_point / 2]))
Plot.draw(mapping)

print 'return to continue, Ctrl C to stop'
raw_input()

# with then all the option of plot except color which is now style and
# you can change as you want (defined are 'r2g', 'g2r', 'b2r'...), you
# can add a wedge to the plot (wedge=1) change the brightness
# (brightness=0-(0.5)-1) or contrast (contrast=0-(1))
Plot.draw(mapping, style='b2r', wedge=1)

print 'return to continue, Ctrl C to stop'
raw_input()

# You can also define 'physical' unit for your plot and still use
# limitsX/Y and aspect
Plot.draw(mapping, sizeX=[-1, 1], sizeY=[
          -2, 2], limitsY=[-1, 1], aspect=1, wedge=1)

print 'return to continue, Ctrl C to stop'
raw_input()

mapping_array = []
n_map = 365
for i in range(n_map):
    mapping_array.append(
        Numeric.absolute(RandomArray.standard_normal([120, 120])))

MultiPlot.draw(range(n_map), mapping_array, wedge=1)
# with the same options again except aspect

print 'return to continue, Ctrl C to stop'
raw_input()

# Contours


def dist(x, y):
    return (x - 125) ** 2 + (y - 125) ** 2  # distance from point (5,5) squared

image = Numeric.sqrt(Numeric.fromfunction(dist, (200, 200))) - 50

Plot.draw(image, wedge=1, aspect=1, style='rainbow')
Plot.draw(image, doContour=1, overplot=1)
Plot.contour['color'] = 2
Plot.contour['linewidth'] = 10

print 'return to continue, Ctrl C to stop'
raw_input()

Plot.draw(image, doContour=1, overplot=1, levels=[-10, 10, 20, 30])

print 'end of demonstration'
