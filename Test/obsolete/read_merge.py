from boa.fortran import fStat
import copy
import os
from boa import BoaPointing
from Numeric import *
import BoaMapping
import cPickle
from slalib import sla_gmst, sla_e2h, sla_eqeqx


# p    = BoaPointing.Point()

def read_sz(data):
    data.BolometerArray.readRCPfile('ASZCaJan2006.rcp')
    data.BolometerArray.UsedChannels = array(
        [23, 35, 36, 48, 49, 50, 62, 63, 64, 65,
         77, 78, 79, 80, 81, 93, 94, 95, 96, 97,
         98, 110, 111, 112, 113, 114, 115, 116,
         128, 129, 130, 131, 132, 133, 134, 135,
         147, 148, 149, 150, 151, 152, 153, 154,
         155, 167, 168, 169, 170, 171, 172,
         173, 174, 175, 176], Int)
    data.BolometerArray._flagChannels(range(332), -1)
    data.BolometerArray._flagChannels(data.BolometerArray.UsedChannels, 0)
    data.BolometerArray.NUsedChannels = 55
    data.BolometerArray.RefChannel = 114
    # Also define telescope
    data.BolometerArray.Telescope.set(name="APEX", diameter=12.0,
                                      longitude=-67.75922, latitude=-23.00575,
                                      elevation=5105.0)
    # Data at 2 mm
    data.BolometerArray.EffectiveFrequency = 3.e8 / 2.e-3
    data.BolometerArray.computeBeamSize()

    nc = shape(data.Data)[1]
    data.Cor_Mat = zeros((nc, nc), Float32)
    data.FFCF_CN = zeros((nc, nc), Float32)
    data.FFCF_Gain = zeros(nc, Float32)


def read_sz_old(data):

    data.BolometerArray.readRCPfile('ASZCa.rcp')
    # Also define telescope
    data.BolometerArray.Telescope.set(name="APEX", diameter=12.0,
                                      longitude=-67.75922, latitude=-23.00575,
                                      elevation=5105.0)
    # Data at 2 mm
    data.BolometerArray.EffectiveFrequency = 3.e8 / 2.e-3
    data.BolometerArray.computeBeamSize()
    #

    #
    data.BolometerArray.UsedChannels = []
    #
    data.BolometerArray._flagChannels(range(332), -1)
    # Select used edge, and update offsets, using Rudiger's text file
    off = file('rcp/pixeloffsets.txt')
    lines = off.readlines()
    off.close()
    nb = len(lines)
    # offsets of the reference channel (113)
    x_ref = -122
    y_ref = -1070

    # use the so-called SZBE numbering, for direct mapping with the data
    for i in range(3, nb):  # skip first 3 lines - text
        tmp = lines[i].split()
        num = int(tmp[1])
        data.BolometerArray._flagChannels([num], 0)
        data.BolometerArray.UsedChannels.append(num)
        data.BolometerArray.Offsets[0, num - 1] = 60. * float(tmp[2]) - x_ref
        data.BolometerArray.Offsets[1, num - 1] = 60. * float(tmp[3]) - y_ref

    data.BolometerArray.RefChannel = 7  # i.e.  bolo. 132
    nc = shape(data.Data)[1]
    data.Cor_Mat = zeros((nc, nc), Float32)


def doubleBeamMap(data):
    """ by rotating the array by 180., produce a map where the bolometers
    appear separated by twice their actual sepataions - works fine with
    a map observation, where all bolometers have seen a point source """

    # use all read data
    nbChan = shape(data.Data)[1]
    for i in range(nbChan):
        mm = fStat.basicmedian(data.Data[:, i])
        data.Data[:, i] = data.Data[:, i] - mm

    data.fastMap(style='gray')


def read_file(Source='mars'):

    # TODO: adapt the path to your local copy!
    # sz_dir = '/aux/pc20029a/schuller/APEX/SZ/'
    # sz_dir = '/home/beelen/OpenBOA/'

    sz_dir = 'Test/'
    if Source == 'saturn':
        f = file(
            sz_dir +
            'Saturn-Raster-NoFrontends-2005-12-24T06:36+0000.ac.mjd_rk-100Hz.mrg')
        # f =
        # file(sz_dir+'SaturnAzScanOffset850-1DegPerSec-2005-12-24T06_52+0000.ac.mjd.mrg')
    else:
        # Raster on Mars (Scan 40302)
        # Observed on 2005 Dec. 25, 02:06 UTC
        # Az = -29.7, El = 45.5
        # Astro: RA 02:26:57.3230 Dec 16:03:05.933, Flux    437.56
        # command: otf(xlen=1800.0,xstep=9.0,ylen=1800.0,ystep=30.0,time=0.02,
        # direction='x',zigzag=1,unit='arcsec',system='ho',epoch=2000.0)
        f = file(sz_dir + 'mars1-raster-eloffset850-0207-rk-100Hz.ac.mrg')

    rl = f.readlines()
    f.close()

    # We will return the result in a pointing object
    data = BoaPointing.Point()

    mjd = []
    bolo = []
    az = []
    el = []
    ra = []
    dec = []

    # Number of lines
    nb = len(rl)
    # Now read the mapping between chan numbers and columns number in .mrg
    crazy = file('rcp/mb2chanNum.txt')
    corr = crazy.readlines()
    crazy.close()

    numCol = []
    runNum = []
    for i in range(2, len(corr)):
        tmp = corr[i].split()
        numCol.append(int(tmp[2]))
        runNum.append(int(tmp[1]))

    for i in range(nb):
        t = rl[i].split()
        flag = int(t[5])
        # if not flag:
        mjd.append(float(t[0]))
        ra.append(float(t[1]))
        dec.append(float(t[2]))
        az.append(float(t[3]))
        el.append(float(t[4]))
        bb = zeros((55), Float)
        # Only 55 meaningful signals
        # for j in range(6,61):
        #    bb.append(float(t[j]))
        for j in range(55):
            if numCol[j] > 1:
                bb[runNum[j] - 1] = float(t[numCol[j] + 4])

        bolo.append(bb)

    if az[0] < 0:
        az = array(az) + 360.
    # Store data and intialize flags
    data.Data = array(bolo)
    data.DataFlags = zeros(shape(data.Data), Int0)
    data.DataBackup = copy.copy(data.Data)
    data.Weight = ones(shape(data.Data), Float32)

    # Define the bolometer array
    read_sz(data)

    # Store time - use MJD also for LST
    data.ScanParam.MJD = array(mjd)
    # data.ScanParam.LST   = 86400.*(array(mjd)-mjd[0]) # NO, see below
    data.ScanParam.Flags = zeros(shape(data.ScanParam.MJD), Int0)

    # Subscans: there's a new one each time delta t > 2 sec
    dt = array(data.ScanParam.MJD[1:]) - array(data.ScanParam.MJD[:-1])
    dt = 86400. * dt
    nb = len(dt)
    subNum = 1
    data.ScanParam.SubscanNum = [subNum]
    data.ScanParam.SubscanIndex = []
    prevNum = 0
    for i in range(nb):
        if dt[i] > 2:
            subNum += 1
            data.ScanParam.SubscanNum.append(subNum)
            data.ScanParam.SubscanIndex.append([prevNum, i])
            prevNum = i + 1
    data.ScanParam.SubscanIndex.append([prevNum, nb + 1])
    data.ScanParam.SubscanIndex = transpose(data.ScanParam.SubscanIndex)

    # ra, dec position of Saturn (fixed)
    # as given by astro: RA 08:52:03.1289 Dec 18:13:21.499
    if Source == 'saturn':
        ra_0 = 15. * (8. + 52. / 60. + 3.1289 / 3600.)
        de_0 = 18. + 13. / 60. + 21.499 / 3600.

    else:
        # Mars, Scan 40302
        # Astro: RA 02:26:57.3230 Dec 16:03:05.933, Flux    437.56
        ra_0 = 15. * (2. + 26. / 60. + 57.3230 / 3600.)
        de_0 = 16. + 3. / 60. + 5.933 / 3600.

    ra_0 = ra_0 * pi / 180.                  # everything in radian
    de_0 = de_0 * pi / 180.
    # Convert ra, dec, az, el to az_off, el_off
    # first, convert mjd to lst
    lst = []
    nb = len(mjd)
    for i in range(nb):
        gmst = sla_gmst(mjd[i])          # Greenwich mean sidereal time
        gast = gmst + sla_eqeqx(mjd[i])  # Greenwich apparent sidereal time
        lst.append(gast + data.BolometerArray.Telescope.Longitude * pi / 180)

    # Then hour angle is lst - ra
    ha = array(lst) - ra_0
    # Convert ha, dec to Az, El position of Saturn
    az_0 = []
    el_0 = []
    for i in range(nb):
        az0, el0 = sla_e2h(ha[i], de_0,
                           data.BolometerArray.Telescope.Latitude * pi / 180.)
        az_0.append(az0 * 180. / pi)         # convert to degree
        el_0.append(el0 * 180. / pi)
        if az_0 < 0:
            az_0 += 360.

    data.ScanParam.Az = array(az)
    data.ScanParam.El = array(el)
    data.ScanParam.Baslon = array(ra)
    data.ScanParam.Baslat = array(dec)
    data.ScanParam.Lon = array(az) - array(az_0)
    data.ScanParam.Lat = array(el) - array(el_0)
    data.ScanParam.LST = array(lst) * (180. / pi) * (86400. / 360.)

    # dump the raw data
    if Source == 'saturn':
        data.ScanParam.Object = 'Saturn'
        data.ScanParam.ScanNum = 40283
        data.dumpData(sz_dir + 'SZ_saturn.dat')
        # data.dumpData(sz_dir+'SZ_saturn2.dat')
    else:
        data.ScanParam.Object = 'Mars'
        data.ScanParam.ScanNum = 40302
        data.dumpData(sz_dir + 'SZ_mars.dat')

    return data

# Reload dumped data object


def reload(Source='saturn'):
    # Initialise a Pointing object - will be returned
    p = BoaPointing.Point()
    sz_dir = 'Test/'

    try:
        if Source == 'saturn':
            fileName = sz_dir + 'SZ_saturn.dat'
            f = file(fileName)
        elif Source == 'saturn2':
            fileName = sz_dir + 'SZ_saturn2.dat'
            f = file(fileName)
        else:
            fileName = sz_dir + 'SZ_mars.dat'
            f = file(fileName)
    except IOError:
        p.MessHand.error(" could not open file %s" % (fileName))
        return
    tmp = cPickle.load(f)  # Here it is of class Map
    f.close()

    # Copy data to the Pointing object
    p.Data = copy.copy(tmp.Data)
    p.DataFlags = copy.copy(tmp.DataFlags)
    p.BolometerArray = copy.copy(tmp.BolometerArray)
    p.ScanParam = copy.copy(tmp.ScanParam)

    return p


def map1(data):
    # do something with the data
    # data.statistics()
    # data.MessHand.info("Statistics computed")
    # data.basePoly()
    data.medianBaseline()
    data.flagChannels([12])  # Noisy channel

    # Flag source before subtracting baseline / subscan
    data.despike(below=-5., above=2.)
    data.basePolySubscan(order=1)
    data.despike(below=-5., above=1.)
    data.basePolySubscan(order=2)
    data.unflag(flag=2)

    # And do a wonderful map
    # data.BolometerArray.rotateArray(187.)  #TODO: find the correct angle!
    data.fastMap(range(17), oversamp=5., noPlot=1)
    data.showMap(style='idl5', labelX="\gD Az ['']", labelY="\gD El ['']",
                 caption='ASZCa - Source: Saturn - Scan: 40283')


def dopoint(p):
    p.fastMap(range(17), oversamp=5., noPlot=1)
    p.solvePointing(
        range(17),
        Xpos=0.,
     Ypos=0.,
     radius=100.,
     plot=1,
     gradient=0)


def sub_snf(data):
    # Let's try and remove some correlated skynoise
    refChan = 5
    data.BolometerArray.NChannels = 26  # dirty FIX !!!!
    data.corMatrix()
    data.plotCorrel(chanRef=refChan, chanList=range(17))
    # correlate: TO BE FIXED!!!!
    # data.correlate(channel=refChan,plot=1)   # compute flatfield
    print "To subtract skynoise, press <enter>"
    raw_input()

    #
    # snf requires FFCF_CN to be computed (using correlate)...
    # I give up!!
    #
    # sig(range(17))
    # for i_ch in data.BolometerArray.UsedChannels:
    # data.snf(channel=i_ch,subtract=1)  # compute skynoise
    # sig(range(17),ci=7,overplot=1)
    # raw_input()
    #

#
# Note: ASTRO gives F_nu(150 GHz) = 658.56 Jy for Saturn
# at the date/time of this observation
#


def point_bolo(p, bolo=2, Xpos=21.5, Ypos=-7.2):
    # Solve pointing for individual bolo
    p.fastMap([bolo], oversamp=5.)
    p.solvePointing([bolo], Xpos=Xpos, Ypos=Ypos, radius=200., plot=1)
    # Intens. / (FWHM_x * FWHM_y) = 2087.38

    # Noise estimate:
    bg = p._Map__Results[250:300, 190:240]
    # Plot.draw(bg,wedge=1)
    mm, rms = fStat.basicrms(bg)
    print "Bolo: " + str(bolo) + " rms = " + str(rms) + " [arb. unit]"
    # bolo2: rms = 1.265 -> 399 mJy

    # bolo3: Intens. / (FWHM_x * FWHM_y) = 2468.66
    # rms = 2.009 -> 536 mJy

    # bolo 4: Intens. / (FWHM_x * FWHM_y) = 1719.25
    # rms = 0.759 -> 291 mJy

    # bolo 5:Intens. / (FWHM_x * FWHM_y) = 1561.54
    # rms = 0.846 -> 357 mJy

    # Combine these first 4 channels
    # p.solvePointing([2,3,4,5],Xpos=178.,Ypos=64.,radius=300.)
    # Intens. / (FWHM_x * FWHM_y) = 1168.0
    # rms = 1.192 -> 672 mJy

    # Or combine only # 4 and 5
    # p.solvePointing([4,5],Xpos=178.,Ypos=64.,radius=300.)
    # Intens. / (FWHM_x * FWHM_y) = 1580.29
    # rms = 0.761 -> 317 mJy


def noise(data, bolo, sizeX=[-1000, -700], sizeY=[-100, 200]):
    # Do maps per bolo of a small region which should be only noise
    #
    # If only one bolo, convert it to a list
    if isinstance(bolo, type(1)):
        bolo = [bolo]

    bolo = data.BolometerArray.checkChanList(bolo)
    data.fastMap(bolo, oversamp=3., sizeX=sizeX, sizeY=sizeY,
                 style='gray', caption='Bolo ' + str(bolo))

    mm, rr = fStat.basicrms(data.Map.data)
    data.MessHand.info("Bolo: " + str(bolo) + " RMS = " + str(rr))
