import profile
data.read('../fits/APEX-420.fits')
readR('SIMBA.rcp')
op('/XWINDOW')
profile.run("data.slowMap2()")
myreduce = data.Results
profile.run("data.slowMap()")
hisreduce = data.Results

plot.draw(myreduce, nan=1)
plot.draw(hisreduce, nan=1)
