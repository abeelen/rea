from Numeric import *
import BoaDataEntity
import BoaChannelAnalyser
from .Bogli import BogliBusiness


myBogli = BogliBusiness.BogliB()

myDataEntity = BoaDataEntity.DataEntity(myBogli)
myDataEntity.read('../Fits/2417.fits')

myDataEntity.Bogli.DevHand.openDev('/XWINDOW')
myDataEntity.signal()

myChanAna = BoaChannelAnalyser.ChanAna.__init__(myDataEntity)
