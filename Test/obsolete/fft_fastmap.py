# Automatically adapted for numpy.oldnumeric Nov 22, 2012 by

import numpy.oldnumeric.fft as FFT
import cPickle
from ppgplot import *


def module(c):
    """ Return module squared of a complex number """
    return c.real * c.real + c.imag * c.imag

data.read('../Fits/fastmap.fits')
# Beginning and end of first subscan
ind1 = data.Subscan_Index[0, 0] - 1
# ind2 = data.Subscan_Index[1,0]-1
# don't know why, but seems to be -2
ind2 = data.Subscan_Index[1, 0] - 2
# extract data for one bolo
d = data.Data_Red_p[ind1:ind2, 0, 8]
nbData = shape(d)[0]
# compute FFT
f = FFT.fft(d)
f = f[:nbData / 2 + 1]  # keep only positive frequencies
m = module(f)
# scale data to get power law fits with exponents -1/2,...
m = pow(m, 4. / 11.)

# Time between first and last data points
total = data.LST[ind2] - data.LST[ind1]
nu1 = 1. / total  # first frequency of FFT in Hz
# frequency axis;
freq = nu1 * array(range(1, nbData / 2 + 1))

# op('/xw')
op('fft_fastmap.gif/gif')
plot.plot(freq, m,
          labelX='Frequency (Hz)', labelY='Power density (arb.u.)',
          caption='MAMBO fast mapping observation - ' +
          'Scan 9855 - Source: Moon',
          limitsX=[0.01, 100], limitsY=[4., 2.5e5], logX=1, logY=1)

# indicate 50 Hz
plot.plot([50., 50.], [600, 3000], overplot=1, style='l')
pgptxt(1.7, 3.7, 0, 0.5, '50 Hz')

# exponents
# slope=[-1.15,-17./4.,-11.,-11./4.]
slope = [-1. / 2., -3 / 2., -4., -1.]
x0 = 1.
y0 = 2.21
xfit = 10. ** array([x0 + 0.85, x0, x0 - 0.36, x0 - 0.62, x0 - 1.82])
yfit = 10. ** array([y0 + 0.85 * slope[0], y0, y0 - 0.36 * slope[1],
                     y0 - 0.36 * slope[1] - 0.26 * slope[2],
                     y0 - 0.36 * slope[1] - 0.26 * slope[2] - 1.20 * slope[3]])
plot.plot(xfit, yfit, overplot=1, style='l', ci=2, width=10)
clo(1)
