# filter.py - example script showing the use of FilterFFT

# the class is in Utilities, so import it
from .Utilities import FilterFFT

# Read a datafile
BoaConfig.projID = 'X-00.F-0001-2006'
read('283')

# Do someting on signal in channel 116
chan = data.BolometerArray.getChannelIndex(116)
xx = data.getChanData('mjd')    # get the timestamps
yy = data.Data[:, chan]           # get the signal

fft = FilterFFT(xx, yy)           # instanciate an fft object
fft.rebin()                      # rebin to regular time steps
fft.doFFT()                      # compute fft

fft.blankAmplitude(below=1.45, above=1.39)  # blank the 1.4 Hz spike
newSig = fft.invFFT()           # compute inverse FFT
plot(yy - newSig)                  # plot the difference

# Now compare total(fft) for a normal channel and a noisy one
ch1 = data.BolometerArray.getChannelIndex(116)
ch2 = data.BolometerArray.getChannelIndex(128)

y1 = data.Data[:, ch1]
y2 = data.Data[:, ch2]
fft1 = FilterFFT(xx, y1)
fft2 = FilterFFT(xx, y2)
fft1.rebin()
fft1.doFFT()
fft2.rebin()
fft2.doFFT()
sum1 = sum(fft1.amplitude[1:])
sum2 = sum(fft2.amplitude[1:])
stat()
print "Chan 116: sum(fft) = ", sum1, " chanRms = ", data.ChanRms[ch1]
print "Chan 128: sum(fft) = ", sum2, " chanRms = ", data.ChanRms[ch2]

raw_input()

# If you wanna plot the FFT in log/log:
# fft.u contains the frequencies, fft.amplitude the amplitudes
plot(fft.u[1:], fft.amplitude[1:], logY=1, logX=1, style='l')

#
# Let's do it for all channels
chanList = data.BolometerArray.checkChanList([])
sumFFT = []
for chan in chanList:
    ch = data.BolometerArray.getChannelIndex(chan)
    yy = data.Data[:, ch]
    fft = FilterFFT(xx, yy)
    fft.rebin()
    fft.doFFT()
    # Remove 0-frequency
    sumFFT.append(sum(fft.amplitude[1:]))
