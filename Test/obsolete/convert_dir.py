from . import MamboMBFits
import os

inDir = '../MamboSS05/'
outDir = '../MBFitsSS05/'

listing = os.listdir(inDir)
fitsfile = [filename for filename in listing if filename[-5:] == ".fits"]
for filename in fitsfile:
    translate = MamboMBFits.MamboMBFits(inDir + filename, outDir + filename)
    translate.convertMambo2MBFits()
