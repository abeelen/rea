#!/usr/bin/env python
#
# doLaboca.py
#
# Read LABOCA simulated data from an ASCII file and generate a
# complete MB-Fits file, that can be read in BoA
#
# author   : F. Schuller - MPIfR
#            adapted from compSimba.py
# 20050608 - will now generate MB-Fits 1.55 files

try:
    from mbfits import *
except ImportError:
    from BoaMBfits import *
import cPickle
from Numeric import *
from math import pi
import string
from slalib import *
import random


def log2int(val):
    """
    DES: Utility to convert logical values from string ('F' or 'T')
         to integer (0 or 1), as required by updateKeyword.
    """

    if (val == 'F'):
        val = 0
    elif (val == 'T'):
        val = 1
    return val


def createFebe(newTab, rcpName, scannum):
    fptr = newTab.fptr

    # Create the Header of the new table
    XmlStruc = XmlStructure('MBFITS')
    table = XmlStruc.XmlScan.tables[newTab.Name]
    # read table properties
    Type = int(table.getAttribute('type'))
    # create an empty FITS-table (will be filled after Keywords are filled)
    status = cfitsio.fits_create_tbl(fptr, Type, 0, 0, [], [], [], newTab.Name)
    # get XML-Object corresponding to the header keywords of the table
    tblHeader = table.getElementsByTagName('HEADER')[0]
    # create Keywords from XML-Object
    newTab.Header.create(tblHeader.childNodes)

    nbFeed, theRow = computeFebepar(rcpName)
    # Put correct values for numbers of bands and feeds
    newTab.Header.updateKeyword('NUSEBAND', '', 1)
    newTab.Header.updateKeyword('FEBEFEED', '', nbFeed)
    newTab.Header.updateKeyword('NUSEFEED', '', nbFeed)
    # Also update Scan number and FEBE name
    newTab.Header.updateKeyword('SCANNUM', '', scannum)
    # febe = rcpName.ljust(8)+'-ABBAAPEX'
    febe = 'LABOCA  -ABBAAPEX'
    newTab.Header.updateKeyword('FEBE', '', febe)

    # Now create the Binary table
    # get XML-Object corresponding to the Columns of the table
    TblCols = table.getElementsByTagName('TABLE')[0]
    newTab.BinTable.create(TblCols.childNodes)
    # create binary table columns, and update units and descriptions

    for i in xrange(len(newTab.BinTable.ttypes)):
        if newTab.BinTable.descriptions[i] != '':
            status = cfitsio.fits_insert_col(fptr, i + 1,
                                             newTab.BinTable.ttypes[i], newTab.BinTable.tforms[i])
        if newTab.BinTable.tdims[i] != '':
            DimKey = 'TDim' + str(i + 1)
            status = cfitsio.fits_update_key(fptr, 16, DimKey,
                                             newTab.BinTable.tdims[i], 'dimension of field')
        ttypeNr = 'TTYPE' + str(i + 1)
        status = cfitsio.fits_update_key(fptr, 16, ttypeNr,
                                         newTab.BinTable.ttypes[i], newTab.BinTable.descriptions[i])
        status = cfitsio.fits_write_key_unit(fptr, ttypeNr,
                                             newTab.BinTable.units[i])
        # newTab.BinTable.updateDimension(newTab.BinTable.ttypes[i])

    newTab.BinTable.setVariableLengthDimension('USEFEED', nbFeed, 1)
    newTab.BinTable.setVariableLengthDimension('BESECTS', nbFeed, 1)
    newTab.BinTable.setVariableLengthDimension('FEEDTYPE', nbFeed, 1)
    newTab.BinTable.addTableRow(theRow)

    return febe, nbFeed  # to update content of SCAN-MBFITS column
                        # nbFeed needed to know the size of data


def computeFebepar(fileName):
    """
    DES: This method fills a list with all parameters for one FEBE.
         It returns this list, in a format ready to be written as one row
         in the FEBEPAR-MBFITS table.
    """

    # Read the Receiver Channels Parameters
    rcp = readRCP(fileName + '.rcp')
    numbers = rcp[-1]
    UseBand = [1]                 # new in v. 1.52
    useAC = numbers[1]
    useDC = numbers[2]
    nbFeed = useAC + useDC
    Usefeed = range(1, nbFeed + 1)
    # Usefeed = transpose([Usefeed])
    # Usefeed = [Usefeed]
    # Feedtype = useAC*'A'+useDC*'D'
    # Feedtype = useDC*'D'+useAC*'A'   # say phase = AC, intensitity = DC
    Feedtype = useAC * [1] + useDC * [2]    # say AC=1 and DC=2 (need numbers)

    Off_X, Off_Y, Bolflat = [], [], []
    for i in range(nbFeed):
        Off_X.append(rcp[i][1])
        Off_Y.append(rcp[i][2])
        Bolflat.append(rcp[i][0])
    # Reference Channel
    Reffeed = numbers[0]
    Polty = nbFeed * 'X'

    Pola = list(zeros(nbFeed, 'f'))
    dummy = list(ones(nbFeed, 'f'))
    dumSquare = [dummy]
    for i in range(nbFeed):
        dumSquare.append(dummy)
    Apereff = dummy
    Beameff = dummy
    # forwardEff = self.PrimaryMambo.KeywordDict['FORWEFF'][0]
    forwardEff = 1.
    Etafss = list(forwardEff * ones(nbFeed, 'f'))
    HPBW = dummy
    Antgain = dummy
    Gainimag = dummy
    # New in version 1.54
    bolgain = 1.
    # bechans = dumSquare
    bechans = dummy

    # New in version 1.55
    nusefeed = [nbFeed]
    tcal = dummy
    boldcoff = dummy

    # Put everything in one single list
    oneRow = [UseBand, nusefeed, Usefeed, bechans, Feedtype, Off_X, Off_Y]
    oneRow.extend([Reffeed, Polty, Pola, Apereff, Beameff])
    oneRow.extend(
        [Etafss,
         HPBW,
         Antgain,
         tcal,
         1.,
         bolgain,
         boldcoff,
         Bolflat,
         Gainimag,
         0.,
         0.])

    return nbFeed, oneRow


def readRCP(fileName):
    """
    DES: Read a Receiver Channel Parameters file specified by
         'fileName', and returns a list of tuples, where each tuple
         is (Gain,X_off,Y_off). Append a tuple (CenterPix, nbACPix,
         nbDCPix) at the end.
    """

    try:
        f = file(fileName)
    except IOError:
        print "No file " + fileName + "found in this directory."
        return

    param = f.readlines()
    result = []
    for i in range(len(param) - 1):  # -1: skip last line
        tmp = string.split(param[i])
        if tmp[0] != '!':			# skip comments
            result.append((string.atof(tmp[1]),
                           string.atof(tmp[3]), string.atof(tmp[4])))

    # Read the last line (numbers of AC and DC channels)
    tmp = string.split(param[-1])
    result.append((string.atoi(tmp[1]),
                   string.atoi(tmp[3]), string.atoi(tmp[5])))

    f.close()
    return result


def fillLst(fileName=None, tabNum=5, long=-67.7592):
    """
    DES: Read a DATAPAR Table and fill the LST column, as computed
         from MJD values found in the same table.
    INP: fileName = name of the file to be processed (str)
         tabNum   = HDU number of the DATAPAR table (int)
         long     = longitude (in deg) of the site (float, + for east)
    """

    f = FitsFile(fileName)
    f.open(1)  # read-write mode
    tab = Table(Parent=f)
    tab.read(tabNum)
    print 'numObs = ', tab.Header.readKeyword('OBSNUM')

    mjd = tab.BinTable.Data['MJD']
    nbRow = len(mjd)
    print "From new file, read in ", tab.Name, " Table, mjd: ", nbRow
    for i in xrange(nbRow):
        if (i == 0):
            # convert MJD to Gregorian calendar
            a, s = sla_djcal(9, mjd[i])
            s, b = sla_dd2tf(4, a[3] / 1.e9)
            # compute a string to update DATE-OBS
            date = str("%4i-%02i-%02iT%02i:%02i:%02i.%04i" %
                       (a[0], a[1], a[2], b[0], b[1], b[2], b[3]))
        gmst = 180. * sla_gmst(mjd[i]) / pi   # in degrees
        lst = gmst + long
        if (lst < 0):
            lst = lst + 360.
        elif (lst > 360):
            lst = lst - 360.
        lst = lst * 240.   # in seconds: (24*3600)/360
        tab.BinTable.updateTableColumn('LST', [lst], i + 1)

    # Also write DATE-OBS in all table headers
    tab = Table(Parent=f)
    tab.HduNum = 1
    tab.Name = 'Primary'
    tab.Header.updateKeyword('DATE-OBS', '', date)
    for num in range(tabNum - 3, tabNum + 2):
        # tab = Table(Parent=f)
        tab.HduNum = num
        tab.Name = tab.Header.readKeyword('EXTNAME')
        tab.Name = tab.Name[1:-1]  # remove quotes
        tab.Header.updateKeyword('DATE-OBS', '', date)

    f.close()


def doLaboca(inName="lissajou.txt", outName="lissajou.fits",
             source="", scannum=37, rcpName="LABOCA", noise=0., correl=0.8):
    """
    INP: (str) inName = name of input text file
         (str) outName = name of output FITS file
         (str) source = object name
         (int) scannum = scan number
         (str) rcpName = name of RCP file to store in FEBEPAR table
         (float) noise = amount of noise to add to the data - values with a
                         uniform distribution between -noise/2 and noise/2
         (float) correl = amplitude of a sine wave that will be added as
                          correlated noise (= the same for all bolo)
    """

    # Create the output file
    out = FitsFile("!" + outName)
    out.create()
    primary = Table("Primary", out)

    scan = Table('SCAN-MBFITS', out)
    scan.create()
    scan.Header.updateKeyword('NOBS', '', 1)
    # scannum = 37
    if source:
        scan.Header.updateKeyword('OBJECT', '', source)
    scan.Header.updateKeyword('SCANNUM', '', scannum)
    # generate a more correct FEBEPAR table at this stage
    febe = Table('FEBEPAR-MBFITS', scan)
    fptr = febe.fptr
    febe.HduNum = 3
    febeName, nbFeed = createFebe(febe, rcpName, scannum)
    # Update the value of FEBE in Scan column
    scan.BinTable.updateTableColumn('FEBE', [febeName], 1)
    # Store the FLATFIEL (relative gains)
    gains = febe.BinTable.readTableColumn('FLATFIEL')[0]
    # the shape is (295,1)
    gains = ravel(gains)

    # Now generate ARRAYDATA and DATAPAR tables
    data = Table('ARRAYDATA-MBFITS', febe)
    data.create()
    datapar = Table('DATAPAR-MBFITS', febe)
    datapar.create()
    # update a few keywords in headers
    data.Header.updateKeyword('FEBE', '', febeName)
    datapar.Header.updateKeyword('FEBE', '', febeName)
    data.Header.updateKeyword('SCANNUM', '', scannum)
    datapar.Header.updateKeyword('SCANNUM', '', scannum)
    data.Header.updateKeyword('OBSNUM', '', 1)
    datapar.Header.updateKeyword('OBSNUM', '', 1)
    data.Header.updateKeyword('BASEBAND', '', 1)
    data.Header.updateKeyword('CHANNELS', '', 1)  # needed for data size
    data.Header.updateKeyword('NUSEFEED', '', nbFeed)  # new in v.1.55
    data.BinTable.updateDimension('DATA')
    # need a DATE-OBS in all tables
    primary.Header.updateKeyword('DATE-OBS', '', '2005-06-08T20:41:21')
    scan.Header.updateKeyword('DATE-OBS', '', '2005-06-08T20:41:21')
    febe.Header.updateKeyword('DATE-OBS', '', '2005-06-08T20:41:21')
    data.Header.updateKeyword('DATE-OBS', '', '2005-06-08T20:41:21')
    datapar.Header.updateKeyword('DATE-OBS', '', '2005-06-08T20:41:21')
    # Also put the correct frequency value
    data.Header.updateKeyword('RESTFREQ', '', 3.45e11)

    # Read the input file
    ff = file(inName)
    lines = ff.readlines()
    ff.close()

    # Initialise lists to fill columns at once
    theInteg = []
    theData = []
    theAz = []
    theEl = []
    # it will help to have the LST filled, and Az, El
    theLst = []
    theX = []
    theY = []

    # Write out all data
    # print "Number of lines: ",len(lines),type(len(lines))
    nb = len(lines)
    nn = 0
    for l in lines:
        nn += 1
        tmp = l.split()
        integNum = string.atoi(tmp[0]) + 1
        azOff = string.atof(tmp[1]) / 3600.  # convert to degree
        elOff = string.atof(tmp[2]) / 3600.
        # could be that az,el offsets are wrong in the text files
        # azOff = -1.*string.atof(tmp[1])/3600.  # convert to degree
        # elOff = -1.*string.atof(tmp[2])/3600.

        # multiply signal with relative gains
        tmpData = zeros((295), Float)
        for i in range(295):
            tmpData[i] = string.atof(tmp[3 + i])
            if noise:
                tmpData[i] += random.uniform(-0.5 * noise, 0.5 * noise)
        # correlated noise: sine wave with period so that one integ. = 1 deg,
        # on top of a constant offset = 5 * sine amplitude
        corNoise = 5. * correl + correl * sin(nn * pi / 180.)
        tmpData += corNoise  # same for all bolo
        tmpData = tmpData * gains

        theInteg.append(integNum)
        theData.extend(tmpData)
        theAz.append(azOff)
        theEl.append(elOff)
        theLst.append(4000. + float(nn) / 10.)
        theX.append(135.)
        theY.append(60.)

    # Now write out the columns
    data.BinTable.updateTableColumn('DATA', theData, 1)
    print "data written"

    # datapar.BinTable.updateTableColumn('INTEGNUM',theInteg,1) # no longer
    # exists in 1.55
    datapar.BinTable.updateTableColumn('LONGOFF', theAz, 1)
    datapar.BinTable.updateTableColumn('LATOFF', theEl, 1)
    datapar.BinTable.updateTableColumn('LST', theLst, 1)
    datapar.BinTable.updateTableColumn('AZIMUTH', theX, 1)
    datapar.BinTable.updateTableColumn('ELEVATIO', theY, 1)
    print "DATAPAR updated"

    out.close()
