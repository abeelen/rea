import BoaDir
import BoaMessageHandler

mess = BoaMessageHandler.MessHand('SZ_script')

indir('/aux/apexa/archive-2005/rawdata/X-00.F-0001-2005')
ils()

for f in BoaDir.currentList:
    name = f['filename']
    data.read(name)
    data.flagLST(below=0.)

    # Check if not everything has been flagged
    lst = data.getChanData('lst')
    if len(lst):
        num = data.ScanParam.ScanNum
        # TODO: why is it always 0, starting from the 2nd file read??
        if num == 0:
            mess.warning(" no scan number - using file name: " + name)
            num = name

        # Plot Az, El offsets pattern
        op(str('SZ_%s_azel.gif/gif' % (str(num))))
        data.ScanParam.plotAzElOffset()
        data.ScanParam.plotAzElOffset(ci=2, style='p', overplot=1)
        clo()

        # Plot a map
        op(str('SZ_%s_map.gif/gif' % (str(num))))
        data.fastMap()
        clo()

    else:
        mess.error(" all data flagged!! " + name)
