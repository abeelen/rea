# Automatically adapted for numpy.oldnumeric Nov 22, 2012 by

from Numeric import *
import numpy.oldnumeric.random_array as RandomArray
import numpy.oldnumeric.mlab as MLab
from .Bogli import DeviceHandler, Plot
from . import Utilities
from .fortran import fUtilities


# DeviceHandler.openDev('/XWINDOW')

testShell = []
testQuickSort = []
testMLab = []

nsort = 500
ntest = 10
index = (power(10, (
    arrayrange(nsort, typecode=Float32) / (nsort + 1) * 4 + 1))).astype(Int)

timing = Utilities.Timing()

for i in range(nsort):
    testArray = RandomArray.normal(0, 10, index[i])

    timing.setTime()
    for dummy in range(ntest):
        b = 0
        b = fUtilities.shell(testArray)
    testShell.append(timing.getTime())

    timing.setTime()
    for dummy in range(ntest):
        b = 0
        b = fUtilities.quicksort(testArray)
    testQuickSort.append(timing.getTime())

    timing.setTime()
    for dummy in range(ntest):
        b = 0
        b = MLab.sort(testArray)
    testMLab.append(timing.getTime())


Plot.plot(index, testShell, style='l',
          logX=1, logY=1,
          labelX='size of array to sort',
          labelY='time to sort for %i tests (sec)' % ntest, ci=2)
Plot.plot(index, testQuickSort, style='l', overplot=1, ci=3)
Plot.plot(index, testMLab, style='l', overplot=1)
