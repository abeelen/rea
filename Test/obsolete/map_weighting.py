close('all')
read('42946')
stat()
data.flagRms(below=20)
data.BolometerArray.rotateArray(170)
mapping(limitsZ=[-500, 1200], style='rainbow')
op()
data.flagPosition(radius=50, flag=3)
data._DataAna__statistics()
data.unflag(flag=3)
weight = 1. / data.ChanRms ** 2
# Take care of flagged channels
# Should not
# weight = where(equal(data.ChanRms,-1.0),0,weight)
# Take care of dead channels
# should be flagged before...
# weight = where(equal(data.ChanRms,0),0,weight)

for i in range(0, data.ScanParam.NInt):
    data.DataWeights[i, :] = weight.astype(Float32)

mapping(limitsZ=[-500, 1200], style='rainbow')
