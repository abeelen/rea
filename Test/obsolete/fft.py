# Automatically adapted for numpy.oldnumeric Nov 22, 2012 by

from boa.Bogli import Plot, DeviceHandler
from boa.BoaDataAnalyser import FilterFFT
from Numeric import *
import numpy.oldnumeric.random_array as RandomArray

DeviceHandler.openDev()

# Meaning it works also with non power of 2 array
nData = 2 ** 12 - 4

# define 2 cosine with frequencies 10 and 40 Hz
f1 = 10.
f2 = 20.0

print "Sin 1"
print "Period    : ", 1. / f1, " s"
print "Frequence : ", f1, " Hz"

print "Sin 2"
print "Period    : ", 1. / f2, " s"
print "Frequence : ", f2, " Hz"

X = arange(nData, typecode='float') / (nData - 1) * 3
Y = cos(2 * pi * f1 * X) + cos(2 * pi * f2 * X)
Plot.plot(X, Y, style='l', labelX='time', labelY='signal', limitsX=[0, 1])
Plot.plot(X, Y, style='p', overplot=1)
print "Data without noise (sub-sample)"
print "Press a key to continue"
raw_input()

# + white noise
# Y=Y+RandomArray.normal(0,2,nData)
Plot.plot(X, Y, style='l', labelX='time', labelY='signal', limitsX=[0, 1])
print "Data with noise (sub-sample)"
print "Press a key to continue"
raw_input()


myFFT = FilterFFT(X, Y)
myFFT.rebin()
myFFT.doFFT(optimize=0)
myFFT.plotfft(limitsX=[0, 50])
print "Amplitude FFT of the data"
print "Press a key to continue"
raw_input()

newY = myFFT.invFFT()
Plot.plot(X, (Y - newY), limitsY=[-5e-7, 5e-7],
          style='l', labelX='time', labelY='Diff with original data')
print "Difference between FFT signal and original data"

myFFT.doFFT(optimize=1)
newY = myFFT.invFFT()
Plot.plot(X, (Y - newY), overplot=1, ci=2, style='l')
print "Press a key to continue"
raw_input()
