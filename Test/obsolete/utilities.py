# Automatically adapted for numpy.oldnumeric Nov 22, 2012 by

# File used to test the Utilities module

from . import Utilities
import numpy.oldnumeric as Numeric
from . import Bogli

# Open some device
myDevHand = Bogli.DeviceHandler.DevHand()
myDevHand.openDev('/XWINDOW')
myPlot = Bogli.Plt.Plot()

# Setup the timing routine
myTiming = Utilities.Timing()

x = Numeric.arange(100, typecode=Numeric.Float)
p = Numeric.array([10, 60, -1])
y = Utilities.modelparabola(p, x)
myPlot.plot(x, y)

myTiming.setTime()
result = Utilities.fitParabola(x, y, y / 10)
print "Done in ", myTiming

# 2D Gaussian fitting

# First make some map within boa, for example :
# indir('../fits')
# infile('APEX-420')
# read()
# op('/XWINDOW')
# data.MessHand.setMaxWeight(5)
# data.slowMap()
# Save the map to a file
# myMap = {'elevation': data.elevation,\
# 	 'azimuth': data.azimuth,\
# 	'map': data.Results}
#
# import cPickle
# f = file('myMap.sav','w')
# cPickle.dump(myMap,f)
# f.close()

# Restore the map
import cPickle
f = file('myMap.sav')
myMap = cPickle.load(f)
f.close()

# Draw the map
myPlot.draw(
    myMap['map'],
     nan=1,
     wedge=1,
     aspect=1,
     sizeX=myMap['azimuth'],
     sizeY=myMap['elevation'])

# fit the map
m = Utilities.fitBase2DGauss(
    myMap['map'],
     sizeX=myMap['azimuth'],
     sizeY=myMap['elevation'])

# compute a ellipse corresponding to 2 FWHM to ...
a = Utilities.draw2Dgauss(m['params'])

# ... overplot  on the map
myPlot.plot(a[0], a[1], overplot=1, style='l')
myPlot.plot(
    [m['gauss_x_offset']['value']],
     [m['gauss_y_offset']['value']],
     overplot=1,
     style='p')


# Test the fortran extension
from . import fortran
from . import Utilities
import numpy.oldnumeric as Numeric
import profile


maxAzoff = 200
minAzoff = -200
maxEloff = 200
minEloff = -200

fwhm = 40

dimX = 100
dimY = 100

map_x = Numeric.array(Numeric.transpose([Numeric.arange(dimX * 1.0) / (dimX - 1) *
                                         (maxAzoff - minAzoff) + minAzoff] * dimY))
map_y = Numeric.array([Numeric.arange(dimY * 1.0) / (dimY - 1) *
                       (maxEloff - minEloff) + minEloff] * dimX)

map_x = Numeric.ravel(map_x)
map_y = Numeric.ravel(map_y)

test_array = Numeric.arrayrange(1000.) / 2 - 2500

p = Numeric.array(
    [0,
     0,
     0,
     2 * Numeric.pi * fwhm ** 2 / (8 * Numeric.log(2)),
     0,
     0,
     fwhm,
     fwhm,
     0])
position = Numeric.array([map_x, map_y])

profile.run(
    "for i in range(1000): real_kernel = Utilities.modelBase2Dgauss(p,position)")
real_kernel = Numeric.reshape(real_kernel, (dimX, dimY))

profile.run(
    "for i in range(1000): fortran_kernel = fortran.Utilities.modelbase2dgauss(p,position)")
fortran_kernel = Numeric.reshape(fortran_kernel, (dimX, dimY))

myPlot.draw(real_kernel - fortran_kernel)

# Other test
import profile
import BoaPointing

data = BoaPointing.Point(bogliB)
data.read('spiral1_off')
profile.run("data.solvePointing()")


# Crude test of the gaussian filtering

import numpy.oldnumeric as Numeric
import Image
from .fortran import f90, Utilities

# The image to gaussian blur
dimX = 100
dimY = 100
im = Image.open("foto4.jpg")
im = im.convert('L')
im = im.rotate(-90)
im = im.resize((dimY, dimX))
im = Numeric.reshape(list(im.getdata()), (dimX, dimY))

# define the kernel, the old way
fwhm = 3
kernel_size = int(ceil(fwhm * 1.51)) * 2 + 1
kernel_center = (kernel_size - 1) / 2
kernel = Numeric.zeros((kernel_size, kernel_size), Numeric.Float)

for i in range(kernel_size):
    for j in range(kernel_size):
        kernel[i, j] = (kernel_center - i) ** 2 + (kernel_center - j) ** 2

kernel = Numeric.exp(-2.772588 * kernel / fwhm ** 2)

# The two smoothing function
gsmoothedMap = f90.f1.gsmooth(im, fwhm)
ksmoothedMap = Utilities.ksmooth(im, kernel)
plot.draw(abs((ksmoothedMap - gsmoothedMap) / gsmoothedMap) * 100, wedge=1)
