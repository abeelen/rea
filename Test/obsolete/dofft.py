# Automatically adapted for numpy.oldnumeric Nov 22, 2012 by

import numpy.oldnumeric.fft as FFT
data.read('../Fits/3543.fits')
z = data.Data_Red_p[0:64, 0, 20]
f = FFT.fft(z)


def module(c):
    return c.real * c.real + c.imag * c.imag
m = module(f)
op('module.gif/gif')
plot.plot(range(33), m[0:33], style='l')
clo(1)

data.read('../Fits/APEX-861a.fits')
readR('SIMBA.rcp')
op('map861.gif/gif')
# map limits: to avoid NaN values
data.fastMap(sizeX=[-200, 200], sizeY=[-100, 140])
clo(1)
# exract 32x32 submap
submap = data._Map__Results[16:48, 8:]
f2 = FFT.fft2d(submap)
mod2 = module(f2[0:17, 0:17])  # modules of positive frequencies

op('fft2d.gif/gif')
plot.draw(log(mod2), wedge=1)  # logarithmic scale
clo(1)
