# Automatically adapted for numpy.oldnumeric Nov 22, 2012 by

"""
NAM: Test2.py
DES: test python wrapped f90 methods
     colors: 1=w  2=red 3=green 4=blue 5=l-blue 6=pink 7=yellow
HIS: FB040530
USE:

boa

open('/xserve')
execfile('Test2.py')

"""
from numpy.oldnumeric import array

messw(5)   # set message weight to max verbosity
indir("../Fits/")
infile("3546")
read()  # onoff
unflag()  # set all flags to 0

pha = 1   # select a reference phase and channel
cha = 1

channel(range(17))
        # select channels and phase to plot. Numbering here is n+1 !!!
phase(pha)

stat()  # compute statistics
base(channel=-1, phase=-1, order=1, plot=0, subtract=1)  # fit 0-order baseline
stat()  # recompute statistics

Rms = BoaB.currData.Cha_Rms[pha, cha]
limits(['*', '*', -3 * Rms, 3 * Rms])  # set plot limits
cl()

set(stylePlot, 'p')
set(ciData, 2)
signal()

flag(below=2.5, above=2.5)   # flag all data outside +-2 rms
set(ciData, 1)
signal()

cmatrix()

refChan = 10 - 1
# LST = BoaB.currData.LST_p  # backup copy. now trick to plot:
# BoaB.currData.Data_Bac_p = BoaB.currData.Data_Red_p
# BoaB.currData.LST_p[:,pha] = BoaB.currData.Data_Red_p[:,pha,refChan]
# set(ciData,3)
# signal()
correlate(channel=refChan, phase=pha, plot=0)
# set(stylePlot,'l')
# set(ciData,7)
# signal()
# BoaB.currData.LST_p[:,pha] = LST[:,pha] # recover LST
# BoaB.currData.Data_Red_p = BoaB.currData.Data_Bac_p     #recover Data

snf(channel=refChan, phase=pha, subtract=1)
set(ciData, 6)
set(stylePlot, 'p')
signal()
