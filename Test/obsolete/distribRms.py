BoaDir.findInDir()
ils()
# select('NSubscans',1,'gt')
rms = []
i = 0
for scan in BoaDir.CurrentList:
    if not scan['status']:
        continue
    i += 1
    print str(i) + " / " + str(len(BoaDir.CurrentList))
    filename = scan['filename']
    try:
        read(filename)
        base(order=1)
        stat()
        rms.append(data.getChanListData('rms', range(1, 296)))
    except:
        pass
    if i > 1:
        draw(transpose(rms), wedge=1, style='idl4', limitsZ=[0, 50],
             labelX='Feed number', labelY='Scans', caption='rms (order 1 baseline subtracted)')
