# Strong source (Jupiter)
read('42947')
data.medianBaseline()
mapping(oversamp=3)
data.solvePointingOnMap(plot=1)

raw_input()

# Fainter source (Uranus)
read('46117')
data.medianBaseline()
data.medianNoiseRemoval()
plotRmsChan()
flagRms(above=20)  # or whatever rms seems too high
mapping(oversamp=3)
data.solvePointingOnMap(plot=1)
