# execfile('test-20051109.py')   # type this on the boa prompt
# execfile('test.bogli.py')   # testing plotting commands

open('/xserve')

# op()             # short version
# resi()		 # after you resized the window
# close()		 # close device
# clear()		 # clear device

indir("../fits/")    # this is the default anyhow
read('3543')
data.fastMap()
data.BolometerArray._flagChannels([1, 11, 42, 83, 103, 118, 119, 120])
data.BolometerArray.rotateArray(58.)
data.fastMap(style='gray')
azelo(overplot=1)

infile('APEX-421')
read()
signal(range(1, 10))

data.fft(chanList=[1])

chanmap()
az()
el()
azel()
azo()
elo()
azelo()
plotcor()
data.BolometerArray.plotArray()  # we have to define a shortcut!
data.BolometerArray.plotArray(overplot=1)
