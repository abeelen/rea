read('2632')
flagC(resistor)
flagC(short)
flagRms(below=2e-5)
data.medianBaseline()
op('rms_raw.ps/CPS')
plotRmsChan()
plotRmsChan(gr02, overplot=1, ci=2)
plotRmsChan(gr09, overplot=1, ci=3)
close()

data._DataAna__statistics()
rms = data.ChanRms
for ll in [gr01, gr02, gr03, gr04, gr05, gr06, gr07, gr08, gr09, gr10, gr11, gr12]:
    ll = data.BolometerArray.checkChanList(ll)
    # use channel with lowest rms for reference
    bestChan = ll[0]
    bestRms = rms[bestChan - 1]
    for num in ll[1:]:
        if rms[num - 1] < bestRms:
            bestChan = num
            bestRms = rms[num - 1]
    data.medianNoiseRemoval(ll, chanRef=bestChan)

op('rms_clean.ps/CPS')
plotRmsChan()
plotRmsChan(gr02, overplot=1, ci=2)
plotRmsChan(gr09, overplot=1, ci=3)
close()

base(order=1)
despike()
op('rms_clean2.ps/CPS')
plotRmsChan()
plotRmsChan(gr02, overplot=1, ci=2)
plotRmsChan(gr09, overplot=1, ci=3)
close()
