# test.flags.py
#
# test script to check that flagged data are handled correctly,
# e.g. when plotting correlation between channels

# Create a fake data object, with same data in all channels
# (perfect correlation!)
data.reset()
data.Data = zeros((10, 4), Float32)
for i in range(4):
    for j in range(10):
        data.Data[j, i] = 1.26 + 0.4 * j

data.BolometerArray.UsedChannels = [1, 2, 3, 4]
data.BolometerArray.CurrChanList = array([1, 2, 3, 4])
data.BolometerArray.Flags = zeros((4), Int)
data.DataFlags = zeros((10, 4), Int)

op()
data.plotCorrel(1, style='l')
data.plotCorrel(1, style='p', ci=3, overplot=1)
print "No data flagged, perfect correlation - press <enter>"
raw_input()

# Now flag a few datapoints
data.DataFlags[5, 2] = 1
data.DataFlags[6:9, 3] = 1
data.plotCorrel(1, style='l')
data.plotCorrel(1, style='p', ci=3, overplot=1)

raw_input()
close()
