import BoaPointing

import config
config.DEBUG = 0

p = BoaPointing.Point()

# Create a dictionary to store results, per subscan
focus = {'fwhm_x': [], 'fwhm_y': [], 'intens': [], 'foc_Z': []}

# Bad channels
bad = [1, 11, 42, 103, 118, 119, 120]

for i in range(6):
    p.read('3544', [i + 1])
    angle = p.ScanParam.El[0]
    p.BolometerArray.rotateArray(angle)
    p.flagChannels(bad)

    p.solvePointing(radius=30., plot=0, Xpos=0., Ypos=0., fixedPos=1)
    focus['fwhm_x'].append(p.PointingResult['gauss_x_fwhm']['value'])
    focus['fwhm_y'].append(p.PointingResult['gauss_y_fwhm']['value'])
    focus['intens'].append(p.PointingResult['gauss_int']['value'])
    focus['foc_Z'].append(p.ScanParam.Foc_Z[0])

op()
plot(focus['foc_Z'], focus['fwhm_x'], limitsY=[9., 18.],
     labelX='Focus Z position [mm]', labelY='2D Guassian fit FWHM ["]')
plot(focus['foc_Z'], focus['fwhm_y'], overplot=1, ci=2)

# Amplitude = Integrated intensity / (FWHM_x * FWHM_y)
amp = []
for i in range(6):
    amp.append(focus['intens'][i] / (focus['fwhm_x'][i] * focus['fwhm_y'][i]))
