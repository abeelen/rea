# use of MamboMBFits
#
# you also need two envrionment variables properly set up
# MBFITSXML pointing to the MBFits.xml file
# MAMBOXML pointing to the Mambo.xml file
# and an array parameter file
# MRT_2002s2_40.rcp in this example
#

from .MamboMBFits import *

oldPath = '../fits/Mambo/'
newPath = '../fits/'

# f = MamboMBFits('../Fits/26084','../Fits/6084')
# f.convertMambo2MBFits()

f = MamboMBFits(oldPath + '2417', newPath + '2417')
f.convertMambo2MBFits()
f = MamboMBFits(oldPath + '0822', newPath + '822')
f.convertMambo2MBFits()
f = MamboMBFits(oldPath + '2295', newPath + '2295')
f.convertMambo2MBFits()
