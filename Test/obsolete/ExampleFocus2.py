"""
NAM: ExampleFocus2.py
DES: test solving focus
USE: python
     execfile('Test/ExampleFocus2.py')
"""

#
# Example script to test solving focus
#
#
print 'Example script to test solving focus'
open()
print 'you may resize the window as you like, return to continue'
raw_input()
DeviceHandler.resizeDev()
proj('T-77.F-0002-2006')
print 'Example of a strong source (Jupiter)'
print 'reading in file...'
read('43275')        # Strong source (Jupiter)
print 'solve focus'
solveFocus()         # solve focus
print 'return to continue, Ctrl C to stop'
raw_input()

print 'Example of a fainter source (Uranus)'
print 'reading in file...'
read('46118')        # Fainter source (Uranus)
medianBaseline(subscan=0)
medianNoiseRemoval()
print 'removed median baseline and median noise value'
print 'solve focus'
solveFocus()         # solve focus
print 'return to continue, Ctrl C to stop'
raw_input()
close()
print 'end of example'
