# Automatically adapted for numpy.oldnumeric Nov 22, 2012 by

"""
NAM: TestFB.py
DES: test python wrapped f90 methods
     colors: 1=w  2=red 3=green 4=blue 5=l-blue 6=pink 7=yellow
HIS: FB040502
     FB040509 added stat()
     FB040515 revise, now plot limits set by rms
     FB040516 revise. add cmatrix, rename to TestFB.py
     FB040517 minor adjustments
     FB040521 change plotc->channel,plotp->phase
     FB040522 add focus and pointing examples
     FB040523 add chanmap and adjust to new infile()
USE:

open('/xserve')
execfile('TestFB.py')

"""
from numpy.oldnumeric import array

messw(5)   # set message weight to max verbosity
indir("../Fits/")

#------------------------------------------------------------- FOCUS
infile("3544")
read()
cl()
focus()
print "Tested focus... press <Enter>"
raw_input()
infile("8746")
read()
cl()
focus()
print "Tested focus... press <Enter>"
raw_input()
#------------------------------------------------------------- POINTING

infile("5662")
read()
cl()
point(53)  # pointing using channel 53
print "Testing pointing... press <Enter>"
raw_input()

BoaB.Point.initAccPoint()
cl()                 # clear the screen
point(53, omit=[5, 7])  # omit obs 5 and 7
print "Testing pointing... press <Enter>"
raw_input()

infile("5663")
read()
cl()
point(53)
print "Testing pointing... press <Enter>"
raw_input()
cl()
accpoint()

#------------------------------------------------------------- ONOFF

print "To read an onoff press <Enter>"
raw_input()

messw(5)
indir("../Fits/")
infile("3546")
read()  # onoff
# scale down onoff 3546 channel 20 to get the source signal on the plot:
BoaB.currData.Data_Red_p[:, :, 19] = array(0.03*BoaB.currData.Data_Red_p[:,:, 19])

# BoaB.currData.Data_Bac_p = BoaB.currData.Data_Red_p  # backup the data.
# restore at end

unflag()  # set all flags to 0

pha = 0   # select a reference phase and channel
cha = 2

channel([1, 2, 20, 53])
        # select channels and phase to plot. Numberinig here is n+1 !!!
phase(pha)

stat()  # compute statistics
base(channel=-1, phase=-1, order=1, plot=0, subtract=1)  # fit 0-order baseline
stat()  # recompute statistics

Rms = BoaB.currData.Cha_Rms[pha, cha]
limits(['*', '*', -4 * Rms, 7 * Rms])  # set plot limits
cl()
set(ciData, 2)
signal()

flag(below=2, above=2)   # flag all data outside +-2 rms
set(ciData, 1)
signal()

cmatrix()   # compute correlation matrix

addpoly(channel=-1, phase=pha, poly=[3., 1., 0., 2.])   # add a polynomial
set(ciData, 3)
signal()

base(channel=-1, phase=pha, order=6, plot=1, subtract=0)
     # fit 5th-order polynomial
set(ciData, 6)
signal()

print "To read map press <Enter>"
raw_input()

# infile("2417")   # large map
infile("822")    # 16 sub map
# infile("2295")   # 3subs absorber map
read()
chan("all")
cl()
chanmap()
