from rea.Utilities import as_column_major_storage

def checkDelay(channel=110, delay=16, limitsX=[], direction='azoff', limitsY=[]):
    offsets = data.getChanData(direction, channel)
    flux  = data.getChanData('flux', channel)
    plot(offsets, flux, limitsX=limitsX, limitsY=limitsY, style='l')
    if delay > 0:
        plot(offsets[:-delay], flux[delay:], overplot=1, ci=2, style='l')
    elif delay < 0:
        plot(offsets[-delay:], flux[:delay], overplot=1, ci=2, style='l')

def correctDelay(data, delay=16):

    if delay > 0 :
        scanParam = data.ScanParam
    
        scanParam.LST                 = as_column_major_storage(scanParam.LST[:-delay])
        scanParam.MJD                 = as_column_major_storage(scanParam.MJD[:-delay])
        scanParam.Az                  = as_column_major_storage(scanParam.Az[:-delay])
        scanParam.El                  = as_column_major_storage(scanParam.El[:-delay])
        scanParam.RA                  = as_column_major_storage(scanParam.RA[:-delay])
        scanParam.Dec                 = as_column_major_storage(scanParam.Dec[:-delay])
        scanParam.AzOff               = as_column_major_storage(scanParam.AzOff[:-delay])
        scanParam.ElOff               = as_column_major_storage(scanParam.ElOff[:-delay])
        scanParam.RAOff               = as_column_major_storage(scanParam.RAOff[:-delay])
        scanParam.DecOff              = as_column_major_storage(scanParam.DecOff[:-delay])
        scanParam.LonOff              = as_column_major_storage(scanParam.LonOff[:-delay])
        scanParam.LatOff              = as_column_major_storage(scanParam.LatOff[:-delay])
        scanParam.BasLon              = as_column_major_storage(scanParam.BasLon[:-delay])
        scanParam.BasLat              = as_column_major_storage(scanParam.BasLat[:-delay])
        scanParam.LonPole             = as_column_major_storage(scanParam.LonPole[:-delay])
        scanParam.LatPole             = as_column_major_storage(scanParam.LatPole[:-delay])
        scanParam.Rot                 = as_column_major_storage(scanParam.Rot[:-delay])
        scanParam.ParAngle            = as_column_major_storage(scanParam.ParAngle[:-delay])
#        scanParam.Flags               = as_column_major_storage(scanParam.Flags[:-delay])
        scanParam.UT                  = as_column_major_storage(scanParam.UT[:-delay])
        scanParam.FocX                = as_column_major_storage(scanParam.FocX[:-delay])
        scanParam.FocY                = as_column_major_storage(scanParam.FocY[:-delay])
        scanParam.FocZ                = as_column_major_storage(scanParam.FocZ[:-delay])
        scanParam.PhiX                = as_column_major_storage(scanParam.PhiX[:-delay])
        scanParam.PhiY                = as_column_major_storage(scanParam.PhiY[:-delay])
        scanParam.NInt                = scanParam.NInt - delay
        scanParam.SubscanIndex[1, -1] -= delay
        data.ScanParam                = scanParam
        
        data.Data                     = as_column_major_storage(data.Data[delay:,:])
        data.CorrelatedNoise          = as_column_major_storage(data.CorrelatedNoise[delay:,:])
        data.DataWeights              = as_column_major_storage(data.DataWeights[delay:,:])
        
    elif delay < 0 :
        scanParam = data.ScanParam
        
        scanParam.LST                 = as_column_major_storage(scanParam.LST[-delay:])
        scanParam.MJD                 = as_column_major_storage(scanParam.MJD[-delay:])
        scanParam.Az                  = as_column_major_storage(scanParam.Az[-delay:])
        scanParam.El                  = as_column_major_storage(scanParam.El[-delay:])
        scanParam.RA                  = as_column_major_storage(scanParam.RA[-delay:])
        scanParam.Dec                 = as_column_major_storage(scanParam.Dec[-delay:])
        scanParam.AzOff               = as_column_major_storage(scanParam.AzOff[-delay:])
        scanParam.ElOff               = as_column_major_storage(scanParam.ElOff[-delay:])
        scanParam.RAOff               = as_column_major_storage(scanParam.RAOff[-delay:])
        scanParam.DecOff              = as_column_major_storage(scanParam.DecOff[-delay:])
        scanParam.LonOff              = as_column_major_storage(scanParam.LonOff[-delay:])
        scanParam.LatOff              = as_column_major_storage(scanParam.LatOff[-delay:])
        scanParam.BasLon              = as_column_major_storage(scanParam.BasLon[-delay:])
        scanParam.BasLat              = as_column_major_storage(scanParam.BasLat[-delay:])
        scanParam.LonPole             = as_column_major_storage(scanParam.LonPole[-delay:])
        scanParam.LatPole             = as_column_major_storage(scanParam.LatPole[-delay:])
        scanParam.Rot                 = as_column_major_storage(scanParam.Rot[-delay:])
        scanParam.ParAngle            = as_column_major_storage(scanParam.ParAngle[-delay:])
        scanParam.Flags               = as_column_major_storage(scanParam.Flags[-delay:])
        scanParam.UT                  = as_column_major_storage(scanParam.UT[-delay:])
        scanParam.FocX                = as_column_major_storage(scanParam.FocX[-delay:])
        scanParam.FocY                = as_column_major_storage(scanParam.FocY[-delay:])
        scanParam.FocZ                = as_column_major_storage(scanParam.FocZ[-delay:])
        scanParam.PhiX                = as_column_major_storage(scanParam.PhiX[-delay:])
        scanParam.PhiY                = as_column_major_storage(scanParam.PhiY[-delay:])
        scanParam.NInt                = scanParam.NInt - delay
        scanParam.SubscanIndex[1, -1] -= delay
        data.ScanParam                = scanParam
        
        data.Data                     = as_column_major_storage(data.Data[:delay:,:])
        data.CorrelatedNoise          = as_column_major_storage(data.CorrelatedNoise[:delay:,:])
        data.DataFlags                = as_column_major_storage(data.DataFlags[:delay:,:])
        data.DataWeights              = as_column_major_storage(data.DataWeights[:delay:,:])


    from . import ReaFlagHandler
    dataShape = shape(data.Data)
    data.FlagHandler = ReaFlagHandler.createFlagHandler(zeros(dataShape, Int8))

# data.read('43297')
# base(subscan=1,order=1)
# checkDelay(channel=244,delay=32)
# correctDelay(data,delay=32)
