from boa.fortran import fUtilities, fStat


# 
# Standard pipeline reduction
#
# FS - 2006/11/29
#
def standard(order=1, flagMin=5., flagMax1=2., flagMax2=3., radius=0.):

    # read (so far) best RCP
    data.BolometerArray.updateRCP('jup-44830-32-improved.rcp')

    # if radius, flag source
    if radius:
        data.flagPosition(radius=radius)

    # subtract baseline
    if order > 0:
        base(order=order)
    elif order == 0:
        data.medianBaseline()

    # read BE channels info, flag non-bolo
    exec(compile(open('cabling.py').read(), 'cabling.py', 'exec'))
    flagC(resistor)
    # data.unflagChannels([132])
    # flagC(unconnect)
    flagC(short)

    # flag low and high rms channels
    data._DataAna__statistics()
    rms = data.ChanRms
    goodRms = data.getChanListData('rms')
    med_rms = fStat.f_median(goodRms)
    flagRms(above = med_rms*flagMax1)
    flagRms(below = med_rms/flagMin)

    if radius:
        unflag(flag=5)

    # Do correlated noise removal per group
    for ll in [gr01, gr02, gr03, gr04, gr05, gr06, gr07, gr08, gr09, gr10, gr11, gr12]:
        ll = data.BolometerArray.checkChanList(ll)
        # use channel with lowest rms for reference
        bestChan = ll[0]
        bestRms = rms[bestChan - 1]
        for num in ll[1:]:
            if rms[num-1] < bestRms:
                bestChan = num
                bestRms = rms[num-1]
        # print "best chan = ",bestChan
        # sig(ll)
        data.medianNoiseRemoval(ll, chanRef=bestChan)
        # sig(ll,overplot=1,ci=2)
        # raw_input()

    # despiking
    if radius:
        data.flagPosition(radius=radius)
    despike()

    # flag noisy channels
    data._DataAna__statistics()
    goodRms = data.getChanListData('rms')
    med_rms = fStat.f_median(goodRms)
    flagRms(above = med_rms*flagMax2)

    # compute weights for the map
    weight = 1./data.ChanRms**2
    for i in range(0, data.ScanParam.NInt):
        data.DataWeights[i,:] = weight.astype(Float32)
    if radius:
        data.unflag(flag=5)

    # produce map
    mapping(oversamp=5, noPlot=1)
    data.Map.smoothBy(6.)
    data.Map.display(caption=data.ScanParam.caption())
    
# 
# One pipeline reduction
#
def reduc(low,hi1,hi2,radius=100,nbRebin=1,chanRef=115):
    """
    DES: a typical sequence of redcution steps
    INP: low    = flagRms below this
         hi1    = flagRms above this at beginning
         hi2    = flagRms above this at the end
         radius = flag area within this radius from the centre
         nbRebin= number of 2-rebinning passes
         chanRef= reference channel for medianNoiseRemoval
    """
    for i in range(nbRebin):
        data.rebin()
    base(order=1)
    flagRms(below=low)
    flagRms(above=hi1)
    if radius:
        data.flagPosition(radius=radius)
    data.medianNoiseRemoval(chanRef=chanRef)
    base(order=3)
    data.despike()
    data.flagRms(above=hi2)
    if radius:
        data.unflag(flag=5)
    weight = 1./data.ChanRms**2
    for i in range(0, data.ScanParam.NInt):
        data.DataWeights[i,:] = weight.astype(Float32)

# 
# Utilities from other scripts
#
def shift(nb=16,d=None):
    """ Correct for shift between backend and APECS timestamps
    """
    if nb > 0:
        d.ScanParam.AzOff = fUtilities.as_column_major_storage(d.ScanParam.AzOff[:-nb])
        d.ScanParam.ElOff = fUtilities.as_column_major_storage(d.ScanParam.ElOff[:-nb])
        d.ScanParam.Flags = fUtilities.as_column_major_storage(d.ScanParam.Flags[:-nb])
        d.ScanParam.MJD   = fUtilities.as_column_major_storage(d.ScanParam.MJD[:-nb])
        d.ScanParam.RA    = fUtilities.as_column_major_storage(d.ScanParam.RA[:-nb])
        d.ScanParam.Dec   = fUtilities.as_column_major_storage(d.ScanParam.Dec[:-nb])
        d.ScanParam.SubscanIndex[1, -1] -= nb
        d.ScanParam.NInt  -= nb
        d.Data            = fUtilities.as_column_major_storage(d.Data[nb:,:])
        d.DataFlags       = fUtilities.as_column_major_storage(d.DataFlags[nb:,:])
        d.DataWeights     = fUtilities.as_column_major_storage(d.DataWeights[nb:,:])
    elif nb < 0:
        d.ScanParam.AzOff = fUtilities.as_column_major_storage(d.ScanParam.AzOff[-nb:])
        d.ScanParam.ElOff = fUtilities.as_column_major_storage(d.ScanParam.ElOff[-nb:])
        d.ScanParam.Flags = fUtilities.as_column_major_storage(d.ScanParam.Flags[-nb:])
        d.ScanParam.MJD   = fUtilities.as_column_major_storage(d.ScanParam.MJD[-nb:])
        d.ScanParam.RA    = fUtilities.as_column_major_storage(d.ScanParam.RA[-nb:])
        d.ScanParam.Dec   = fUtilities.as_column_major_storage(d.ScanParam.Dec[-nb:])
        d.Data            = fUtilities.as_column_major_storage(d.Data[:nb,:])
        d.DataFlags       = fUtilities.as_column_major_storage(d.DataFlags[:nb,:])
        d.DataWeights     = fUtilities.as_column_major_storage(d.DataWeights[:nb,:])
        
# 
# Examples of use
#
def doSgr():
    indir('../Laboca/trunk')
    read('45121')
    shift(24, d=data)
    data.BolometerArray.updateRCP('jup-44830-32.rcp')
    reduc(2, 80, 16)
    data.doMap(system='EQ', sizeX=[266.78, 266.895], sizeY=[-28.5, -28.31], oversamp=5, noPlot=1)
    map1 = copy.deepcopy(data.Map)

    read('45122')
    shift(24, d=data)
    data.BolometerArray.updateRCP('jup-44830-32.rcp')
    reduc(2, 80, 16)
    data.doMap(system='EQ', sizeX=[266.78, 266.895], sizeY=[-28.5, -28.31], oversamp=5, noPlot=1)
    map2 = copy.deepcopy(data.Map)

    read('45123')
    shift(24, d=data)
    data.BolometerArray.updateRCP('jup-44830-32.rcp')
    reduc(2, 80, 16)
    data.doMap(system='EQ', sizeX=[266.78, 266.895], sizeY=[-28.5, -28.31], oversamp=5, noPlot=1)
    map3 = copy.deepcopy(data.Map)

    map1.Data     = map1.Data     + map2.Data     + map3.Data 
    map1.Weight   = map1.Weight   + map2.Weight   + map3.Weight
    map1.Coverage = map1.Coverage + map2.Coverage + map3.Coverage 
    return map1

