# Co-adding the data
read('scan1')
# processing

# optionally:
data.dumpData('file1.data')

read('scan2')
# processing

# optionally:
data.dumpData('file2.data')

# co-adding the data:

d1 = newRestoreData('file1.data')
d2 = newRestoreData('file2.data')

dsum = d1 + d2
dsum.doMap()

dsum.signal(110)

# co-adding maps (2 maps with same size and same coordinates):

d1.doMap(sizeX=[-300, 300], sizeY=[-200, 200], oversamp=5)
m1 = copy.deepcopy(d1.Map)
d2.doMap(sizeX=[-300, 300], sizeY=[-200, 200], oversamp=5)
m2 = copy.deepcopy(d2.Map)

mm = copy.deepcopy(m1)
nX, nY = shape(mm.Data)

for i in range(nX):
    for j in range(nY):
        mm.Data[i, j] = (m1.Weight[i, j] * m1.Data[i, j] + m2.Weight[
                         i, j] * m2.Data[i, j]) / (m1.Weight[i, j] + m2.Weight[i, j])


mm.display()
mm.computeRms(cell=5)
