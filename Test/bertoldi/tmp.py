
read('4451')
# refChan = 94
refChan = 46

updateRCP('ASZCa_feedmap_2007.rcp', scale=10)
data.BolometerArray.RefChannel = refChan
mapping([refChan], oversamp=3.0)

data.solvePointingOnMap([refChan], plot=1)

data.solvePointing([refChan], plot=1, radius=0)

medianBase(subscan=0)
data.flagInTime('lst', below=0)
BogliConfig.point['size'] = 0.01

signal([102], style='p')

plotarray(limitsY=[-700, 700], limitsX=[-700, 700], num=1)
plotarray(limitsY=[-100, 100], limitsX=[-100, 100], num=1)

#

read('4453')
flagCh([143])
updateRCP('ASZCa_feedmap_2007.rcp', scale=10)read('4451')read('4451')
updateRCP('ASZCa_feedmap_2007.rcp', scale=20)
data.BolometerArray.RefChannel = 94
mapping([94], oversamp=2.0)
data.solvePointingOnMap([94], plot=1)
data.solvePointing([94], plot=1, radius=0)

updateRCP('ASZCa_feedmap_2007.rcp', scale=20)
data.BolometerArray.RefChannel = 94
mapping([94], oversamp=2.0)
data.solvePointingOnMap([94], plot=1)
data.solvePointing([94], plot=1, radius=0)

data.BolometerArray.RefChannel = 46
# medianBase(subscan=0)


solveFocus()

signal(range(40, 50))

data.solvePointing(
    plot=1,
     radius=-10,
     Xpos=Xpos,
     Ypos=Ypos,
     circular=0,
     chanList=[ChanRef])


#
read('4534')
updateRCP('aszca-4522.rcp')
medianBase(subscan=0)
plotRmsChan()
flagRms(below=0.4)


data.BolometerArray.readAsciiRcp(filename='beams_2_4522.txt')
data.BolometerArray.readAsciiRcp(filename='beams_2_4536.txt')
data.BolometerArray.readAsciiRcp(filename='beams_2_4539.txt')

cc = data.BolometerArray.checkChanList([])
execfile('Test/computeRCP2.py')
plotRCP1(cc, '4522', angle=9)


good = [29, 37, 46, 85, 86, 93, 94, 102, 103, 159, 214, 215]
oversamp = 4
for refChan in good:
    mapping([refChan], oversamp=oversamp, noPlot=1, style='idl2')
    data.solvePointingOnMap(plot=1, style='idl2')
    print refChan
    raw_input()
    data.solvePointing([refChan], plot=1, radius=0)
    raw_input()


data1 = copy.deepcopy(data)

mediannoise()
medianbase()
computeWeight()
mapping(sizeX=[-1100, 1100], limitsZ=[-1, 2], style='idl4', oversamp=2)

data.Map.smoothBy(80)
data.Map.display(limitsX=[-1100, 1100], limitsZ=[
                 -1, 2], style='idl4', caption="scan=4544 RXCJ1347 smooth=80")
