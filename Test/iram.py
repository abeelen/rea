from .fortran.fMap import wcs2pix
from .fortran import fStat


def inversion(self, Niter=10, kappa=2, oversamp=3):
    """ build map by matrix inversion method (see Johnstone et al. 2000)
    this is an iterative process, where the positions matrix applied to
    a guessed map is compared to the measured data

    """

    self.Map.Data = zeros(shape(self.Map.Data), 'f')
    for i in range(Niter):
        print "iteration ", i
        iterate(self)
        # singleIter(self)
        # singleEasy(self)

    # Compute and print rms/beam
    m = data.Map.meanDistribution(cell=oversamp)
    m2, nb = fStat.clipping(m, kappa)
    m2 = m2[:nb]
    print "r.m.s. / beam = %f" % (fStat.f_stat(m2)[1])


def iterateBak(self):
    """ compute a new guess using difference with observed signal
    """
    self.Map.display()
    # TODO: include weighting
    chanList = self.BolometerArray.checkChanList([])
    guess = self.Map.Data
    newGuess = copy.deepcopy(guess)
    # newGuess  = zeros(shape(guess),'f')
    # newWeight = self.Map.Weight
    newWeight = zeros(shape(newGuess), 'f')
    # accumulate diff by bolo in this:
    diff = zeros(shape(newGuess), 'f')

    wob = self.ScanParam.WobThrow * 3600.
    throw = int(abs(wob / self.Map.WCS['CDELT1']))
    Map = self.Map
    AXIS1 = array([Map.WCS['NAXIS1'], Map.WCS['CRPIX1'],
                   Map.WCS['CDELT1'], Map.WCS['CRVAL1']])
    AXIS2 = array([Map.WCS['NAXIS2'], Map.WCS['CRPIX2'],
                   Map.WCS['CDELT2'], Map.WCS['CRVAL2']])
    nbX = Map.WCS['NAXIS1']
    nbY = Map.WCS['NAXIS2']
    for chan in chanList:
        chanNum = self.BolometerArray.getChanIndex(chan)[0]
        flux = self.getChanData('flux', chan)
        azOff = self.getChanData('azoff', chan)
        elOff = self.getChanData('eloff', chan)
        for t in range(len(flux)):
            I, J = wcs2pix(azOff[t], elOff[t], AXIS1, AXIS2)
            I = I[0]
            J = J[0]
            Xpos = I + throw / 2
            Xneg = I - throw / 2
            if I - throw / 2 >= 0 and I + throw / 2 < nbX and J >= 0 and J < nbY:
                try:
                    # print Xneg,J
                    if str(guess[Xneg, J]) == str(float('nan')):
                        pass
                        # print "neg ",Xneg,J
                    else:
                        if str(guess[Xpos, J]) == str(float('nan')):
                            pass
                            # print "pos ",Xpos,J
                        else:
                            # dif1 = guess[Xneg,J] + flux[t] # TODO: weights!!
                            # dif2 = guess[Xpos,J] - flux[t]
                            # diff[Xpos,J] += array((dif1),'f')
                            # diff[Xneg,J] += array((dif2),'f')
                            # newWeight[Xpos,J] += array((1.),'f')
                            # newWeight[Xneg,J] += array((1.),'f')

                            dif1 = flux[t] - (
                                guess[Xneg,
                                      J] - guess[Xpos,
                                                 J])  # TODO: weights!!
                            diff[Xpos, J] += array((dif1 / 2.), 'f')
                            diff[Xneg, J] -= array((dif1 / 2.), 'f')
                            newWeight[Xpos, J] += array((1.), 'f')
                            newWeight[Xneg, J] += array((1.), 'f')
                except OverflowError:
                    print I, J
                    print newGuess[I, J], guess[Xneg, J], guess[Xpos, J]
                    raise

    # draw(diff,style='idl4',wedge=1)
    # print "diff..."
    # raw_input()
    # print "weight..."
    # draw(newWeight,style='idl4',wedge=1)
    # raw_input()

    print "max(diff) =", max(abs(ravel(diff)))
    for i in range(nbX):
        for j in range(nbY):
            if newWeight[i, j] > 0:
                try:
                    # newGuess[i,j] = (diff[i,j] / newWeight[i,j])
                    newGuess[i, j] = newGuess[i, j] - (
                        diff[i, j] / newWeight[i, j])
                    # the sky should always be >0
                    if newGuess[i, j] < 0:
                        newGuess[i, j] = 0.
                except:
                    print i, j
                    print self.Map.Data[i, j], newGuess[i, j], newWeight[i, j]
                    raise

    # draw(newGuess,style='idl4',wedge=1)
    print "done"
    self.Map.Data = newGuess
    self.Map.display()
    # raw_input()


def iterate(self):
    """ compute a new guess using difference with observed signal
    """
    self.Map.display()
    # TODO: include weighting
    chanList = self.BolometerArray.checkChanList([])
    guess = self.Map.Data
    newGuess = copy.deepcopy(guess)
    # newGuess  = zeros(shape(guess),'f')
    # newWeight = self.Map.Weight
    newWeight = zeros(shape(newGuess), 'f')
    # accumulate diff by bolo in this:
    diff = zeros(shape(newGuess), 'f')

    wob = self.ScanParam.WobThrow * 3600.
    throw = int(abs(wob / self.Map.WCS['CDELT1']))
    Map = self.Map
    AXIS1 = array([Map.WCS['NAXIS1'], Map.WCS['CRPIX1'],
                   Map.WCS['CDELT1'], Map.WCS['CRVAL1']])
    AXIS2 = array([Map.WCS['NAXIS2'], Map.WCS['CRPIX2'],
                   Map.WCS['CDELT2'], Map.WCS['CRVAL2']])
    nbX = Map.WCS['NAXIS1']
    nbY = Map.WCS['NAXIS2']
    for chan in chanList:
        chanNum = self.BolometerArray.getChanIndex(chan)[0]
        flux = self.getChanData('flux', chan)
        azOff = self.getChanData('azoff', chan)
        elOff = self.getChanData('eloff', chan)
        for t in range(len(flux)):
            I, J = wcs2pix(azOff[t], elOff[t], AXIS1, AXIS2)
            I = int(I[0] + 0.5)
            J = int(J[0] + 0.5)
            Xpos = I + throw / 2
            Xneg = I - throw / 2
            if I - throw / 2 >= 0 and I + throw / 2 < nbX and J >= 0 and J < nbY:
                try:
                    # print Xneg,J
                    if str(guess[Xneg, J]) == str(float('nan')):
                        pass
                        # print "neg ",Xneg,J
                    else:
                        if str(guess[Xpos, J]) == str(float('nan')):
                            pass
                            # print "pos ",Xpos,J
                        else:
                            # dif1 = guess[Xneg,J] + flux[t] # TODO: weights!!
                            # dif2 = guess[Xpos,J] - flux[t]
                            # diff[Xpos,J] += array((dif1),'f')
                            # diff[Xneg,J] += array((dif2),'f')
                            # newWeight[Xpos,J] += array((1.),'f')
                            # newWeight[Xneg,J] += array((1.),'f')

                            dif1 = flux[t] - (
                                guess[Xneg,
                                      J] - guess[Xpos,
                                                 J])  # TODO: weights!!
                            diff[Xpos, J] += array((dif1 / 2.), 'f')
                            diff[Xneg, J] -= array((dif1 / 2.), 'f')
                            newWeight[Xpos, J] += array((1.), 'f')
                            newWeight[Xneg, J] += array((1.), 'f')
                except OverflowError:
                    print I, J
                    print newGuess[I, J], guess[Xneg, J], guess[Xpos, J]
                    raise

    # draw(diff,style='idl4',wedge=1)
    # print "diff..."
    # raw_input()
    # print "weight..."
    # draw(newWeight,style='idl4',wedge=1)
    # raw_input()

    print "max(diff) =", max(abs(ravel(diff)))

    for i in range(nbX):
        for j in range(nbY):
            if newWeight[i, j] > 0:
                try:
                    # newGuess[i,j] = (diff[i,j] / newWeight[i,j])
                    newGuess[i, j] = newGuess[i, j] - (
                        diff[i, j] / newWeight[i, j])
                    # the sky should always be >0
                    # if newGuess[i,j] < 0:
                    #    newGuess[i,j] = 0.
                except:
                    print i, j
                    print self.Map.Data[i, j], newGuess[i, j], newWeight[i, j]
                    raise

    # Now limit negative pixels to -1*rms
    # Distribution of flux/beam
    cell = 3  # this assumes oversamp=3 - TODO: oversamp must be known here
    nX = nbX - cell
    nY = nbY - cell
    allMean, nbOk = fStat.meandistribution(newGuess, nX, nY, nX * nY, cell)
    allMean = allMean[:nbOk]
    # clip data at more than 2-sigma (e.g. a source)
    m2, nbOk = fStat.clipping(allMean, 2.)
    m2 = m2[:nbOk]
    rms = fStat.f_stat(m2)[1]
    for i in range(nbX):
        for j in range(nbY):
            if newGuess[i, j] < -1. * rms:
                newGuess[i, j] = -1. * rms

    # draw(newGuess,style='idl4',wedge=1)
    print "done"
    self.Map.Data = newGuess
    self.Map.display()
    # raw_input()


def singleIter(self):
    """ same as iterate, for single beam data (no wobbler) """
    self.Map.display()
    # TODO: include weighting
    chanList = self.BolometerArray.checkChanList([])
    guess = self.Map.Data
    newGuess = copy.deepcopy(guess)
    newWeight = zeros(shape(newGuess), 'f')
    # accumulate diff by bolo in this:
    diff = zeros(shape(newGuess), 'f')

    Map = self.Map
    AXIS1 = array([Map.WCS['NAXIS1'], Map.WCS['CRPIX1'],
                   Map.WCS['CDELT1'], Map.WCS['CRVAL1']])
    AXIS2 = array([Map.WCS['NAXIS2'], Map.WCS['CRPIX2'],
                   Map.WCS['CDELT2'], Map.WCS['CRVAL2']])
    nbX = Map.WCS['NAXIS1']
    nbY = Map.WCS['NAXIS2']
    for chan in chanList:
        chanNum = self.BolometerArray.getChanIndex(chan)[0]
        flux = self.getChanData('flux', chan)
        weight = self.getChanData('weight', chan)
        azOff = self.getChanData('azoff', chan)
        elOff = self.getChanData('eloff', chan)
        allI, allJ = wcs2pix(azOff, elOff, AXIS1, AXIS2)
        for t in range(len(flux)):
            I, J = allI[t], allJ[t]
            intI = int(I)
            intJ = int(J)
            fracI = I - intI
            fracJ = J - intJ
            if intI >= 0 and intI < nbX - 1 and intJ >= 0 and intJ < nbY - 1:
                try:
                    if str(guess[intI, intJ]) != str(float('nan')):
                        dif1 = weight[t] * (
                            flux[t] * (1. - fracI) * (1. - fracJ) - guess[intI, intJ])
                        # dif1 = weight[t] * (flux[t] -
                        # guess[intI,intJ])*(1.-fracI)*(1.-fracJ)
                        diff[intI, intJ] += array((dif1), 'f')
                        # newWeight[intI,intJ] +=
                        # array((weight[t]*(1.-fracI)*(1.-fracJ)),'f')
                        newWeight[intI, intJ] += array((weight[t]), 'f')
                    if str(guess[intI + 1, intJ]) != str(float('nan')):
                        dif1 = weight[t] * (
                            flux[t] * fracI * (1. - fracJ) - guess[intI + 1, intJ])
                        # dif1 = weight[t] * (flux[t] -
                        # guess[intI+1,intJ])*fracI*(1.-fracJ)
                        diff[intI + 1, intJ] += array((dif1), 'f')
                        # newWeight[intI+1,intJ] +=
                        # array((weight[t]*fracI*(1.-fracJ)),'f')
                        newWeight[intI + 1, intJ] += array((weight[t]), 'f')
                    if str(guess[intI, intJ + 1]) != str(float('nan')):
                        dif1 = weight[t] * (
                            flux[t] * (1. - fracI) * fracJ - guess[intI, intJ + 1])
                        # dif1 = weight[t] * (flux[t] -
                        # guess[intI,intJ+1])*(1.-fracI)*fracJ
                        diff[intI, intJ + 1] += array((dif1), 'f')
                        # newWeight[intI,intJ+1] +=
                        # array((weight[t]*(1.-fracI)*fracJ),'f')
                        newWeight[intI, intJ + 1] += array((weight[t]), 'f')
                    if str(guess[intI + 1, intJ + 1]) != str(float('nan')):
                        dif1 = weight[t] * (
                            flux[t] * fracI * fracJ - guess[intI + 1, intJ + 1])
                        # dif1 = weight[t] * (flux[t] -
                        # guess[intI+1,intJ+1])*fracI*fracJ
                        diff[intI + 1, intJ + 1] += array((dif1), 'f')
                        # newWeight[intI+1,intJ+1] +=
                        # array((weight[t]*fracI*fracJ),'f')
                        newWeight[intI + 1, intJ + 1] += array(
                            (weight[t]), 'f')
                except OverflowError:
                    print I, J
                    print newGuess[I, J], guess[Xneg, J], guess[Xpos, J]
                    raise
                except:
                    print I, J
                    raise

    print "max(diff) =", max(abs(ravel(diff)))
    for i in range(nbX):
        for j in range(nbY):
            if newWeight[i, j] > 0:
                try:
                    # newGuess[i,j] = (diff[i,j] / newWeight[i,j])
                    newGuess[i, j] = newGuess[i, j] + (
                        diff[i, j] / newWeight[i, j])
                    # the sky should always be >0
                    # if newGuess[i,j] < 0:
                    #    newGuess[i,j] = 0.
                except:
                    print i, j
                    print self.Map.Data[i, j], newGuess[i, j], newWeight[i, j]
                    raise

    # draw(newGuess,style='idl4',wedge=1)
    print "done"
    self.Map.Data = newGuess
    self.Map.display()
    # raw_input()


def singleEasy(self):
    """ same as singleIter, without the complication of fractions of pixels """
    self.Map.display()
    chanList = self.BolometerArray.checkChanList([])
    guess = self.Map.Data
    newGuess = copy.deepcopy(guess)
    newWeight = zeros(shape(newGuess), 'f')
    # accumulate diff by bolo in this:
    diff = zeros(shape(newGuess), 'f')

    Map = self.Map
    AXIS1 = array([Map.WCS['NAXIS1'], Map.WCS['CRPIX1'],
                   Map.WCS['CDELT1'], Map.WCS['CRVAL1']])
    AXIS2 = array([Map.WCS['NAXIS2'], Map.WCS['CRPIX2'],
                   Map.WCS['CDELT2'], Map.WCS['CRVAL2']])
    nbX = Map.WCS['NAXIS1']
    nbY = Map.WCS['NAXIS2']
    for chan in chanList:
        chanNum = self.BolometerArray.getChanIndex(chan)[0]
        flux = self.getChanData('flux', chan)
        weight = self.getChanData('weight', chan)
        azOff = self.getChanData('azoff', chan)
        elOff = self.getChanData('eloff', chan)
        allI, allJ = wcs2pix(azOff, elOff, AXIS1, AXIS2)
        for t in range(len(flux)):
            I, J = int(allI[t] + 0.5), int(allJ[t] + 0.5)
            if I >= 0 and I < nbX and J >= 0 and J < nbY:
                try:
                    if str(guess[I, J]) != str(float('nan')):
                        dif1 = weight[t] * (flux[t] - guess[I, J])
                        diff[I, J] += array((dif1), 'f')
                        newWeight[I, J] += array((weight[t]), 'f')
                except OverflowError:
                    print I, J
                    print newGuess[I, J], guess[Xneg, J], guess[Xpos, J]
                    raise
                except:
                    print I, J
                    raise

    print "max(diff) =", max(abs(ravel(diff)))
    for i in range(nbX):
        for j in range(nbY):
            if newWeight[i, j] > 0:
                try:
                    newGuess[i, j] = newGuess[i, j] + (
                        diff[i, j] / newWeight[i, j])
                    # newGuess[i,j] = diff[i,j] / newWeight[i,j]
                    # the sky should always be >0
                    # if newGuess[i,j] < 0:
                    #    newGuess[i,j] = 0.
                except:
                    print i, j
                    print self.Map.Data[i, j], newGuess[i, j], newWeight[i, j]
                    raise

    self.Map.Data = newGuess
    self.Map.display()


#
# Run at startup ###
# Read a file and basic reduction steps


# indir('../../fits/IRAM')

# read('iram30m-abba-20061015s213-imb.fits')
# read('iram30m-abba-20061015s217-imb.fits')  ## Pointing

# read('iram30m-abba-20061015s219-imb.fits',subscans=range(1,32))
# read('iram30m-abba-20061015s223-imb.fits',subscans=range(1,32))

# read('iram30m-abba-20061015s230-imb.fits',subscans=range(1,32))

# read('iram30m-abba-20061015s231-imb.fits',subscans=range(1,32))
# read('iram30m-abba-20061015s235-imb.fits',subscans=range(1,32))

# indir('../../fits/IRAM')
# read('iram30m-abba-20061015s231-imb.fits',subscans=range(1,22))
# read('iram30m-abba-20061115s68-imb.fits',subscans=range(1,32))

def basic(radius=0):
    data.medianBaseline()
    data.flagRms(below=100)
    # data.flagRms(above=600)

    data.BolometerArray.RefChannel = 54
    el = data.ScanParam.El
    medEl = el[len(el) / 2]
    data.BolometerArray.rotateArray(-1. * medEl)
    data.BolometerArray.rotateArray(180.)  # IRAM convention...

    if radius:
        data.flagPosition(radius=radius)
    data.despike()
    data.BolometerArray.RefChannel = 54
    data.medianNoiseRemoval(chanRef=54)
    data.despike()
    # flagMJD(below=30)
    # flagRms(above=240)

    data.computeWeight()
    if radius:
        data.unflag(flag=5)
    mapping(oversamp=3, sizeX=[200, -200], sizeY=[-200, 200])
    data.Map.smoothBy(5.)
    data.Map.display()
