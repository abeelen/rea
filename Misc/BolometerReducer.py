# Automatically adapted for numpy.oldnumeric Nov 22, 2012 by

#
#  			BolometerReducer.py
#
# Copyright (C) 2002-2006
# Max-Planck-Institut fuer Radioastronomie Bonn
#
# Produced for the ALMA and APEX projects
#
# This library is free software; you can redistribute it and/or modify it under
# the terms of the GNU Library General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option) any
# later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Library General Public License for more
# details.
#
# You should have received a copy of the GNU Library General Public License
# along with this library; if not, write to the Free Software Foundation, Inc.,
# 675 Massachusetts Ave, Cambridge, MA 02139, USA.
#
# Correspondence concerning APEX should be addressed as follows:
#
# Internet email: dmuders@mpifr-bonn.mpg.de
#
# Who			When		What
# ----------------------------------------------------------------------------
# D. Muders, MPIfR      16/01/2006      Added baseline fit to pointing
#					reduction.
# F. Schuller, MPIfR    24/10/2005      Created

from boa import BoaPointing, BoaFocus, BoaMapping, BoaConfig
from boa.Bogli import DeviceHandler
import numpy.oldnumeric as Numeric


class BolometerReducer:

    """
    The BolometerReducer provides methods to reduce Bolometer observing modes.
    """

    def __init__(self, online, divMode='normal'):
        self.Online = online
        # set blanking value
        self.blank = -999.0
        # store the overall error status.
        self.errorStatus = {}
        # We will store the scan number and current data object to be able to update
        # when reading a new subscan
        self.CurrentScanNum = 0
        self.CurrentData = None
        # input directory is given by the calibrator
        BoaConfig.inDir = ''

    def reducePoint(
        self,
        scanNum,
     myfile,
     obstoProc=[],
     FeBe='',
     BaseBand=1,
     RefDic={},
     pointingObject=None):
        """
        Reduce a pointing scan and store results in pointingObject
        """

        # start an error status for this scan
        if scanNum not in self.errorStatus.keys():
            self.errorStatus = {}
            self.errorStatus[scanNum] = []

        if not DeviceHandler.DevList:
            # if no open device, open the default one
            DeviceHandler.openDev()

        name = (FeBe, BaseBand)
        fileName = myfile.currentFileName
        if scanNum != self.CurrentScanNum:
            # Initialise a Boa Pointing object
            self.CurrentScanNum = scanNum
            self.CurrentData = BoaPointing.Point()
            self.CurrentData.reduce(fileName, obstoProc, update=0)
        else:
            self.CurrentData.reduce(fileName, obstoProc, update=1)
        tmpRes = self.CurrentData.PointingResult

        # Store results,to be returned to the calling routine
        # If the fit did not converge, tmpRes contains -1
        #
        # FIXME: should use an object rather than -1
        if tmpRes != -1:
            offsets = (
                tmpRes['gauss_x_offset']['value'],
                tmpRes['gauss_y_offset']['value'])
            amplit = (
                tmpRes['gauss_int']['value'],
                tmpRes['gauss_int']['value'])  # only one intensity
            widths = (
                tmpRes['gauss_x_fwhm']['value'],
                tmpRes['gauss_y_fwhm']['value'])
        else:
            offsets = [-999., -999.]
            amplit = [0., 0.]
            widths = [0., 0.]
        # self.Logger.logger.logInfo("Calculating pointing offsets for feed
        # number = %i"%(refFeed))

        pointingObject.object = self.CurrentData.ScanParam.Object
        pointingObject.feed = self.CurrentData.BolometerArray.RefChannel
        # pointingObject.dataUnit = getattr(sciData,'DATAUNIT','')

        return pointingObject, [offsets, amplit, widths]

    def reduceFocus(
        self,
        scanNum,
     myfile,
     obstoProc=[],
     FeBe='',
     BaseBand=1,
     focusObject=None):
        """
        Reduce a focus scan and store results in focusObject
        """
        # start an error status for this scan
        if scanNum not in self.errorStatus.keys():
            self.errorStatus = {}
            self.errorStatus[scanNum] = []

        name = (FeBe, BaseBand)
        focusCount = len(focusObject.offsets)

        self.CurrentScanNum = scanNum
        # Initialise a Boa Focus object
        self.CurrentData = BoaFocus.Focus()
        fileName = myfile.currentFileName
        self.CurrentData.reduce(fileName, obstoProc)

        # Now offsets and fluxes are in ResultOffsets and ResultFluxes
        focusObject.offsets = Numeric.concatenate((focusObject.offsets,
                                                   self.CurrentData.ResultOffsets))
        if Numeric.rank(focusObject.counts) > 1:
            focusObject.counts = Numeric.concatenate((focusObject.counts[:, 0],
                                                      self.CurrentData.ResultFluxes))
        else:
            focusObject.counts = Numeric.concatenate((focusObject.counts,
                                                      self.CurrentData.ResultFluxes))

        # nbPoints = len(self.CurrentData.ResultFluxes)
        nbPoints = len(focusObject.counts)
        focusObject.counts = Numeric.resize(focusObject.counts, [nbPoints, 1])

        # update essential information about this focus:
        focusObject.scan = self.CurrentData.ScanParam.ScanNum
        focusObject.object = self.CurrentData.ScanParam.Object
        # focusObject.feed     = currentData.BolometerArray.RefChannel
        # focusObject.useFeeds = currentData.BolometerArray.UsedChannels
        focusObject.feed = 1  # why should we use any other feed than the ref.??
        focusObject.useFeeds = [1]
        focusObject.dataUnit = 'counts'
        # Get the scan-type to determine axis of focus
        scanType = self.CurrentData.ScanParam.ScanType
        focusObject.axis = scanType[6:]

        return focusObject

    def reduceMap(self, scanNum, myfile, obstoProc, FeBe, BaseBand):
        fileName = myfile.currentFileName
        if scanNum != self.CurrentScanNum:
            # Initialise a Boa Map object
            self.CurrentScanNum = scanNum
            self.CurrentData = BoaMapping.Map()
            self.CurrentData.reduce(fileName, obstoProc, update=0)
        else:
            self.CurrentData.reduce(fileName, obstoProc, update=1)

        return 0

# --------------------------------------------------------------

# This class is also defined in CalibController (but don't import
# this to avoid cyclic import) and in HeterodyneReducer - just
# do the same as in HeterodyneReducer!


class ControllerError(Exception):

    """ A class used to generate exceptions related
    to the CalibController.py module. """

    def __init__(self, msg, value=None):
        self.value = value
        self.msg = msg

    def __str__(self):
        return repr(self.msg)
