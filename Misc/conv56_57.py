from mbfits import *

def conv(inFile, outFile):
    """
    Read file 'inFile' - in MB-Fits ver. 1.56 format - and save its
    content in MB-Fits ver. 1.57 format in 'outFile'
    """
    # Good news is: only a few columns in DATAPAR tables have
    # changed, plus a few new keywords (PHASEi) in SCAN header

    # Read input file
    f = FitsFile(inFile)
    f.open()
    f.read()
    f.close()
    nb = len(f.TableList)

    # initialise output file
    f2 = FitsFile(outFile)
    f2.create()
    primary = Table('Primary', f2)
    primary.HduNum = 1
    Scan = Table('SCAN-MBFITS', f2)
    Scan.create()          # SCAN-MBFITS Table -> HDU = 2
    Febe = Table('FEBEPAR-MBFITS', Scan)
    Febe.create()          # FEBEPAR-MBFITS Table -> HDU = 3

    numTab = 0
    for tab in f.TableList:
        numTab += 1
        # print str("Updating table %2i / %2i"%(numTab,nb))
        if tab.Name == 'Primary':
            copyKey(tab, primary)
            primary.Header.updateKeyword('MBFTSVER', '', '1.57')

        elif tab.Name == 'SCAN-MBFITS':
            copyScan(tab, Scan)
        elif tab.Name == 'FEBEPAR-MBFITS':
            copyFebe(tab, Febe)
        elif tab.Name == 'ARRAYDATA-MBFITS':
            newTab = Table('ARRAYDATA-MBFITS', Febe)
            newTab.create()
            copyData(tab, newTab)
        elif tab.Name == 'DATAPAR-MBFITS':
            newTab = Table('DATAPAR-MBFITS', Febe)
            newTab.create()
            copyPara(tab, newTab)
        elif tab.Name == 'MONITOR-MBFITS':
            newTab = Table('MONITOR-MBFITS', Febe)
            newTab.create()
            copyMonitor(tab, newTab)

        else:
            print "Unknown table: ", tab.Name

    f2.close()

def copyKey(input, output):
    kk = input.Header.Keywords
    for k in kk:
        if k in ['TRANDIST', 'TRANFREQ', 'TRANFOCU', 'DEWANG']:
            output.Header.updateKeyword(k, '', float(input.Header.KeywordDict[k][0]))
        else:
            output.Header.updateKeyword(k, '', log2int(input.Header.KeywordDict[k][0]))

def copyScan(input, output):
    copyKey(input, output)
    # then update PHASEi keywords
    febe = input.BinTable.Data['FEBE']
    if 'MAMBO' in febe:
        output.Header.updateKeyword('PHASE1', '', 'WNEG')
        output.Header.updateKeyword('PHASE2', '', 'WPOS')
    else:
        output.Header.updateKeyword('PHASE1', '', 'None')
    output.BinTable.updateTableColumn('FEBE', febe, 1)

def copyData(input, output):
    copyKey(input, output)
    output.BinTable.updateDimension('DATA')
    mjd = input.BinTable.Data['MJD']
    data = input.BinTable.Data['DATA'][:, 0,:]
    for i in range(len(mjd)):
        output.BinTable.addTableRow([mjd[i], data[i,:]])

def copyFebe(input, output):
    copyKey(input, output)
    # Now the column sizes have to be adjusted
    output.BinTable.updateDimension('FEEDOFFX')
    output.BinTable.updateDimension('FEEDOFFY')
    output.BinTable.updateDimension('POLTY')
    output.BinTable.updateDimension('POLA')
    output.BinTable.updateDimension('APEREFF')
    output.BinTable.updateDimension('BEAMEFF')
    output.BinTable.updateDimension('ETAFSS')
    output.BinTable.updateDimension('HPBW')
    output.BinTable.updateDimension('ANTGAIN')
    output.BinTable.updateDimension('TCAL')
    output.BinTable.updateDimension('BOLDCOFF')
    output.BinTable.updateDimension('FLATFIEL')
    output.BinTable.updateDimension('GAINIMAG')
    # 2D arrays (feeds x band) since v 1.55
    nbUsedFeed = len(input.BinTable.Data['USEFEED'][0])
    output.BinTable.setVariableLengthDimension('USEFEED', nbUsedFeed, 1)
    output.BinTable.setVariableLengthDimension('BESECTS', nbUsedFeed, 1)
    output.BinTable.setVariableLengthDimension('FEEDTYPE', nbUsedFeed, 1)
    # Now copy binary table content
    data = input.BinTable.Data
    oneRow = [data['USEBAND'][0], data['NUSEFEED'][0], data['USEFEED'][0], data['BESECTS'][0], data['FEEDTYPE'][0]]
    oneRow.extend([data['FEEDOFFX'][0], data['FEEDOFFY'][0], data['REFFEED'], data['POLTY'][0], data['POLA'][0]])
    oneRow.extend([data['APEREFF'][0], data['BEAMEFF'][0], data['ETAFSS'][0], data['HPBW'][0], data['ANTGAIN'][0]])
    oneRow.extend([data['TCAL'][0], data['BOLCALFC'][0], data['BOLREFGN'][0], data['BOLDCOFF'][0]])
    oneRow.extend([data['FLATFIEL'][0], data['GAINIMAG'][0], data['GAINELE1'][0], data['GAINELE2'][0]])
    output.BinTable.addTableRow(oneRow)

def copyPara(input, output):
    copyKey(input, output)
    mjd = input.BinTable.Data['MJD']
    for numInteg in range(len(mjd)):
        oneRow = [mjd[numInteg]]
        oneRow.extend([input.BinTable.Data['LST'][numInteg]])
        oneRow.extend([input.BinTable.Data['INTEGTIM'][numInteg]])
        if input.BinTable.Data['ISWITCH'][numInteg] == 'OFF':
            oneRow.extend([2])
        else:
            oneRow.extend([1])
        oneRow.extend([input.BinTable.Data['LONGOFF'][numInteg]])
        oneRow.extend([input.BinTable.Data['LATOFF'][numInteg]])
        oneRow.extend([input.BinTable.Data['AZIMUTH'][numInteg]])
        oneRow.extend([input.BinTable.Data['ELEVATIO'][numInteg]])
        oneRow.extend([input.BinTable.Data['CBASLONG'][numInteg]])
        oneRow.extend([input.BinTable.Data['CBASLAT'][numInteg]])
        oneRow.extend([input.BinTable.Data['BASLONG'][numInteg]])
        oneRow.extend([input.BinTable.Data['BASLAT'][numInteg]])
        oneRow.extend([input.BinTable.Data['ROTANGLE'][numInteg]])
        oneRow.extend([input.BinTable.Data['MCRVAL1'][numInteg]])
        oneRow.extend([input.BinTable.Data['MCRVAL2'][numInteg]])
        oneRow.extend([input.BinTable.Data['MLONPOLE'][numInteg]])
        oneRow.extend([input.BinTable.Data['MLATPOLE'][numInteg]])
        oneRow.extend([input.BinTable.Data['DFOCUS'][numInteg][0]])
        oneRow.extend([input.BinTable.Data['DFOCUS'][numInteg][1]])
        oneRow.extend([input.BinTable.Data['DFOCUS'][numInteg][2]])
        oneRow.extend([input.BinTable.Data['DPHI'][numInteg][0]])
        oneRow.extend([input.BinTable.Data['DPHI'][numInteg][1]])
        oneRow.extend([input.BinTable.Data['DPHI'][numInteg][2]])
        output.BinTable.addTableRow(oneRow)

def copyMonitor(input, output):
    # copy both header and binary table without any change
    copyKey(input, output)
    # Get length of MONVALUE and MONUNITS pointers from input header
    lenValue = input.Header.KeywordDict['TFORM3'][0]
    lenValue = string.split(lenValue, '(')[1]
    lenValue = int(lenValue[:-1])
    lenUnits = input.Header.KeywordDict['TFORM4'][0]
    lenUnits = string.split(lenUnits, '(')[1]
    lenUnits = int(lenUnits[:-1])
    output.BinTable.setVariableLengthDimension('MONVALUE', lenValue)
    output.BinTable.setVariableLengthDimension('MONUNITS', lenUnits)
    # Now copy data row by row
    nbInteg = len(input.BinTable.Data['MJD'])
    for i in range(nbInteg):
        oneRow = [input.BinTable.Data['MJD'][i]]
        oneRow.extend([input.BinTable.Data['MONPOINT'][i]])
        oneRow.extend([input.BinTable.Data['MONVALUE'][i]])
        oneRow.extend([input.BinTable.Data['MONUNITS'][i]])
        output.BinTable.addTableRow(oneRow)

def log2int(val):
    """
    DES: Utility to convert logical values from string ('F' or 'T')
         to integer (0 or 1), as required by updateKeyword.
    """

    if (val == 'F'):
        val = 0
    elif (val == 'T'):
        val = 1
    return val
