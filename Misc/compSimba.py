#!/usr/bin/env python
#
# compSimba.py
#
# Read an APEX-Simba incomplete MB-Fits file, and generate a more
# complete MB-Fits file, that can be read in BoA
#
# authors   : A. Belloche, F. Schuller - MPIfR
# creation  : 2004.05.29 - copy of shortFits_1.py
# version   : 2004.05.29
#             FS20040530 - adapted for several subscans
#             FS20040531 - add writing of approx. offsets

from mbfits import *
import cPickle
from Numeric import *
from math import pi
import string
from slalib import *
import Interpolator
from time import time
import config


def keepObs(
    inName,
     outName,
     listObs=[1],
     nRowsArraydata=0,
     nRowsMonitor=0,
     source=None):
    """
    DES: copy part of an MB-FITS file to a new file, and update the FEBEPAR
    table with (fake) SIMBA channel parameters; also fills a few columns
    with basic data (MJD, INTEGNUM, NINT, INTEGTIM) in DATAPAR tables
    INP: inName (str) name of input file
         outName (str) name of output file
         listObs (list of int) numbers of observations to write
         nRowsArraydata (int) number of rows to write in ARRAYDATA table(s)
                        default: all rows
         nRowsMonitor (int) number of rows to write in MONITOR table(s)
                        default: all rows
         source (str) name of source observed
    OUT: tabWrite (int) number of tables written
    """
# def keepFirst(inName = "APEX-100003-T-00.0-0002-2004.fits",\
#              outName = "APEX-1003-02-2004.fits",\
#              nRowsArraydata = 0, nRowsMonitor = 0):

    tabWrite = 0
    # Read the input file
    f = FitsFile(inName)
    f.open()
    f.read()
    f.close()

    # f2 = file('data.sav','w')
    # cPickle.dump(f,f2,1)
    # f2.close()
    # f2 = file('data.sav')
    # f = cPickle.load(f2)
    # f2.close()

    # Create the output file
    out = FitsFile("!" + outName)
    out.create()
    primary = Table("Primary", out)
    # Copy the content of input file to output
    for k in f.TableList[0].Header.KeywordDict.keys():
        val1 = log2int(f.TableList[0].Header.KeywordDict[k][0])
        primary.Header.updateKeyword(k, '', val1)

    scan = Table('SCAN-MBFITS', out)
    scan.create()
    # Copy content of SCAN-MBFITS table
    for k in f.TableList[1].Header.KeywordDict.keys():
        val1 = log2int(f.TableList[1].Header.KeywordDict[k][0])
        scan.Header.updateKeyword(k, '', val1)
    scannum = int(scan.Header.readKeyword('SCANNUM'))
    # update NOBS keyword
    scan.Header.updateKeyword('NOBS', '', len(listObs))
    if source:
        scan.Header.updateKeyword('OBJECT', '', source)

    # generate a more correct FEBEPAR table at this stage
    febe = Table('FEBEPAR-MBFITS', scan)
    fptr = febe.fptr
    febe.HduNum = 3
    # status = cfitsio.fits_movabs_hdu(fptr,febe.HduNum)
    # print 'move to hdu, status: ',status
    febeName, nbFeed = createFebe(febe, 'SIMBA', scannum)
    # Update the value of FEBE in Scan column
    scan.BinTable.updateTableColumn('FEBE', [febeName], 1)
    print 'febe created: ', febe.Name

    tabWrite += 3

    # store if number of rows = all
    if not nRowsArraydata:
        allData = 1
    else:
        allData = 0
    if not nRowsMonitor:
        allMon = 1
    else:
        allMon = 0

    numObs = 0  # aldo need a counter
    for obs in listObs:
        # should use a getTable method or so
        # number ok for current APEX-Simba files
        curData = obs * 4 - 1
        curDap = obs * 4
        curMon = obs * 4 + 1

        data = Table('ARRAYDATA-MBFITS', febe)
        data.create()
        # Copy content of ARRAYDATA-MBFITS table
        for k in f.TableList[curData].Header.KeywordDict.keys():
            val1 = log2int(f.TableList[curData].Header.KeywordDict[k][0])
            data.Header.updateKeyword(k, '', val1)

        # if no nRowsArraydata input to the method, default = all rows,
        # get it from length or read-in data
        if allData:
            nRowsArraydata = len(f.TableList[curData].BinTable.Data['MJD'])

        print 'starting copy of nRowsArraydata = ', nRowsArraydata
        col = 'MJD'
        mjd = f.TableList[curData].BinTable.Data['MJD'][0:nRowsArraydata]
        print ".... processing ARRAYDATA table, column: ", col
        data.BinTable.updateTableColumn(col, mjd)
        col = 'DATA'
        print ".... processing ARRAYDATA table, column: ", col
        theData = f.TableList[
            curData].BinTable.Data[
                col][
            0:nbFeed * nRowsArraydata]
        # updateTableColumn writes len(data) elements, which in this
        # case is only the number of rows instead of nb rows*nb feeds
        allData = resize(theData, (nbFeed * nRowsArraydata,))
        data.BinTable.updateTableColumn(col, allData, 1)
        # also update febe name in header
        data.Header.updateKeyword('FEBE', '', febeName)

        datapar = Table('DATAPAR-MBFITS', febe)
        datapar.create()
        # Copy content of DATAPAR-MBFITS table
        for k in f.TableList[curDap].Header.KeywordDict.keys():
            value = log2int(f.TableList[curDap].Header.KeywordDict[k][0])
            datapar.Header.updateKeyword(k, '', value)
        datapar.Header.updateKeyword('FEBE', '', febeName)

        # for Simba files without any row in DATAPAR, copy MJD from ARRAYDATA
        datapar.BinTable.updateTableColumn('MJD', mjd)
        # Also fill INTEGNUM, NINTS and INTEGTIM columns
        datapar.BinTable.updateTableColumn(
            'INTEGNUM', range(1, nRowsArraydata + 1), 1)
        datapar.BinTable.updateTableColumn('NINTS', nRowsArraydata * [1], 1)
        diff = (mjd[1:] - mjd[0:-1]) * 86400.   # days -> seconds
        diff = 0.95 * diff       # and 5% overheads
        print 'diff : ', len(diff)
        integ = diff.tolist()
        integ.extend([integ[-1]])    # say last integtim = previous one
        datapar.BinTable.updateTableColumn('INTEGTIM', integ, 1)

        mon = Table('MONITOR-MBFITS', febe)
        mon.create()
        # Copy content of MONITOR-MBFITS table header
        for k in f.TableList[curMon].Header.KeywordDict.keys():
            value = log2int(f.TableList[curMon].Header.KeywordDict[k][0])
            mon.Header.updateKeyword(k, '', value)

        # if no nRowsMonitor input to the method, default = all rows,
        # get it from length or read-in data
        if allMon:
            nRowsMonitor = len(f.TableList[curMon].BinTable.Data['MJD'])
        print 'starting copy of nRowsMonitor = ', nRowsMonitor

        # MONITOR Table doesn't look ok when using updateTableColumn
        # use addTableRows instead
        mjd = f.TableList[curMon].BinTable.Data['MJD'][0:nRowsMonitor]
        list_mjd = mjd.tolist()
        point = f.TableList[curMon].BinTable.Data['MONPOINT']
        list_point = point[0:nRowsMonitor]
        value = f.TableList[curMon].BinTable.Data['MONVALUE']
        units = f.TableList[curMon].BinTable.Data['MONUNITS']
        list_unit = units[0:nRowsMonitor]
        # needed with new readTableColumn method:
        # list_unit = units[0:nRowsMonitor].tolist()
        # convert Az and El from radians to degrees
        for i in xrange(nRowsMonitor):
            if (string.find(list_point[i], 'AZ_EL') >= 0):
                value[i] = value[i] * 180. / pi
                list_unit[i] = ['deg;deg']
            else:
                list_unit[i] = [list_unit[i]]
        list_val = value[0:nRowsMonitor].tolist()

        # Also add rows with weather data
        mon.BinTable.addTableRow([mjd[0], 'TAMB_P_HUMID',
                                  [-4.73, 576.2, 55.3], 'C;mbar;%'])
        mon.BinTable.addTableRow([mjd[0], 'REFRACTIO', [32.3], 'arcsec'])
        a = [list_mjd, list_point, list_val, list_unit]
        mon.BinTable.addTableRows(a)

        tabWrite += 3
        numObs += 1
    # mon.BinTable.addTableRow([mjd[-1],'TAMB_P_HUMID',\
    #                          [-4.82,575.8,56.7],'C;mbar;%'])
    # mon.BinTable.addTableRow([mjd[-1],'REFRACTIO',[34.8],'arcsec'])

    # close output file
    out.close()
    return tabWrite


def log2int(val):
    """
    DES: Utility to convert logical values from string ('F' or 'T')
         to integer (0 or 1), as required by updateKeyword.
    """

    if (val == 'F'):
        val = 0
    elif (val == 'T'):
        val = 1
    return val


def createFebe(newTab, rcpName, scannum):
    fptr = newTab.fptr

    # Create the Header of the new table
    XmlStruc = XmlStructure('MBFITS')
    table = XmlStruc.XmlScan.tables[newTab.Name]
    # read table properties
    Type = int(table.getAttribute('type'))
    # create an empty FITS-table (will be filled after Keywords are filled)
    status = cfitsio.fits_create_tbl(fptr, Type, 0, 0, [], [], [], newTab.Name)
    # get XML-Object corresponding to the header keywords of the table
    tblHeader = table.getElementsByTagName('HEADER')[0]
    # create Keywords from XML-Object
    newTab.Header.create(tblHeader.childNodes)

    nbFeed, theRow = computeFebepar(rcpName)
    # Put correct values for numbers of bands and feeds
    newTab.Header.updateKeyword('NUSEBAND', '', 1)
    newTab.Header.updateKeyword('FEBEFEED', '', nbFeed)  # 5 for Simba
    newTab.Header.updateKeyword('NUSEFEED', '', nbFeed)
    # Also update Scan number and FEBE name
    newTab.Header.updateKeyword('SCANNUM', '', scannum)
    febe = rcpName.ljust(8) + '-ABBAAPEX'
    newTab.Header.updateKeyword('FEBE', '', febe)

    # Now create the Binary table
    # get XML-Object corresponding to the Columns of the table
    TblCols = table.getElementsByTagName('TABLE')[0]
    newTab.BinTable.create(TblCols.childNodes)
    # create binary table columns, and update units and descriptions

    for i in xrange(len(newTab.BinTable.ttypes)):
        if newTab.BinTable.descriptions[i] != '':
            status = cfitsio.fits_insert_col(fptr, i + 1,
                                             newTab.BinTable.ttypes[i], newTab.BinTable.tforms[i])
        if newTab.BinTable.tdims[i] != '':
            DimKey = 'TDim' + str(i + 1)
            status = cfitsio.fits_update_key(fptr, 16, DimKey,
                                             newTab.BinTable.tdims[i], 'dimension of field')
        ttypeNr = 'TTYPE' + str(i + 1)
        status = cfitsio.fits_update_key(fptr, 16, ttypeNr,
                                         newTab.BinTable.ttypes[i], newTab.BinTable.descriptions[i])
        status = cfitsio.fits_write_key_unit(fptr, ttypeNr,
                                             newTab.BinTable.units[i])

    newTab.BinTable.addTableRow(theRow)

    return febe, nbFeed  # to update content of SCAN-MBFITS column
                        # nbFeed needed to know the size of data


def computeFebepar(fileName):
    """
    DES: This method fills a list with all parameters for one FEBE.
         It returns this list, in a format ready to be written as one row
         in the FEBEPAR-MBFITS table.
    """

    # Read the Receiver Channels Parameters
    rcp = readRCP(fileName + '.rcp')
    numbers = rcp[-1]
    UseBand = [1]                 # new in v. 1.52
    useAC = numbers[1]
    useDC = numbers[2]
    nbFeed = useAC + useDC
    Usefeed = range(1, nbFeed + 1)
    # Feedtype = useAC*'A'+useDC*'D'
    Feedtype = useDC * 'D' + useAC * 'A'   # say phase = AC, intensitity = DC
    Off_X, Off_Y, Bolflat = [], [], []
    for i in range(nbFeed):
        Off_X.append(rcp[i][1])
        Off_Y.append(rcp[i][2])
        Bolflat.append(rcp[i][0])
    # Reference Channel
    Reffeed = numbers[0]
    Polty = nbFeed * 'X'

    Pola = list(zeros(nbFeed, 'f'))
    dummy = list(ones(nbFeed, 'f'))
    Apereff = dummy
    Beameff = dummy
    # forwardEff = self.PrimaryMambo.KeywordDict['FORWEFF'][0]
    forwardEff = 1.
    Etafss = list(forwardEff * ones(nbFeed, 'f'))
    HPBW = dummy
    Antgain = dummy
    Gainimag = dummy
    # Put everything in one single list
    oneRow = [UseBand, Usefeed, Feedtype, Off_X, Off_Y]
    oneRow.extend([Reffeed, Polty, Pola, Apereff, Beameff])
    oneRow.extend([Etafss, HPBW, Antgain, 1., Bolflat, Gainimag, 0., 0.])

    return nbFeed, oneRow


def readRCP(fileName):
    """
    DES: Read a Receiver Channel Parameters file specified by
         'fileName', and returns a list of tuples, where each tuple
         is (Gain,X_off,Y_off). Append a tuple (CenterPix, nbACPix,
         nbDCPix) at the end.
    """

    try:
        f = file(fileName)
    except IOError:
        print "No file " + fileName + "found in this directory."
        return

    param = f.readlines()
    result = []
    for i in range(len(param) - 1):  # -1: skip last line
        tmp = string.split(param[i])
        if tmp[0] != '!':			# skip comments
            result.append((string.atof(tmp[1]),
                           string.atof(tmp[3]), string.atof(tmp[4])))

    # Read the last line (numbers of AC and DC channels)
    tmp = string.split(param[-1])
    result.append((string.atoi(tmp[1]),
                   string.atoi(tmp[3]), string.atoi(tmp[5])))

    f.close()
    return result


def fillLst(fileName=None, tabNum=5, long=-67.7592):
    """
    DES: Read a DATAPAR Table and fill the LST column, as computed
         from MJD values found in the same table.
    INP: fileName = name of the file to be processed (str)
         tabNum   = HDU number of the DATAPAR table (int)
         long     = longitude (in deg) of the site (float, + for east)
    """

    f = FitsFile(fileName)
    f.open(1)  # read-write mode
    tab = Table(Parent=f)
    tab.read(tabNum)
    print 'numObs = ', tab.Header.readKeyword('OBSNUM')

    mjd = tab.BinTable.Data['MJD']
    nbRow = len(mjd)
    print "From new file, read in ", tab.Name, " Table, mjd: ", nbRow
    for i in xrange(nbRow):
        if (i == 0):
            # convert MJD to Gregorian calendar
            a, s = sla_djcal(9, mjd[i])
            s, b = sla_dd2tf(4, a[3] / 1.e9)
            # compute a string to update DATE-OBS
            date = str("%4i-%02i-%02iT%02i:%02i:%02i.%04i" %
                       (a[0], a[1], a[2], b[0], b[1], b[2], b[3]))
        gmst = 180. * sla_gmst(mjd[i]) / pi   # in degrees
        lst = gmst + long
        if (lst < 0):
            lst = lst + 360.
        elif (lst > 360):
            lst = lst - 360.
        lst = lst * 240.   # in seconds: (24*3600)/360
        tab.BinTable.updateTableColumn('LST', [lst], i + 1)

    # Also write DATE-OBS in all table headers
    tab = Table(Parent=f)
    tab.HduNum = 1
    tab.Name = 'Primary'
    tab.Header.updateKeyword('DATE-OBS', '', date)
    for num in range(tabNum - 3, tabNum + 2):
        # tab = Table(Parent=f)
        tab.HduNum = num
        tab.Name = tab.Header.readKeyword('EXTNAME')
        tab.Name = tab.Name[1:-1]  # remove quotes
        tab.Header.updateKeyword('DATE-OBS', '', date)

    f.close()


def runInterpol(fileName, listObs=[1]):
    i = Interpolator.Interpolator()
    f = FitsFile(fileName)
    f.open(1)

    # First, read the SCAN-MBFITS table (same for all obs.)
    scan = Table('SCAN-MBFITS', f)
    scan.HduNum = 2
    i.currentScanMbfits = scan
    name = scan.Header.readKeyword('EXTNAME')
    print 'scan = ', name
    # extract SCANNUM and FEBE from its content
    scannum = int(scan.Header.readKeyword('SCANNUM'))
    febe = scan.BinTable.readTableColumn('FEBE')[0]

    # now loop over the observations to find the corresponding
    # tables and run the interpolator
    for obs in listObs:
        tables = f.getObservation(scannum, obs, febe, 1)
        # last parameter (1) = FeBe section
        tables = tables[1:]  # remove SCAN table
        for tabInfo in tables:
            name = tabInfo[1]
            hdu = tabInfo[0]
            if name == 'FEBEPAR-MBFITS':
                i.currentFebeParMbfits = Table('FEBEPAR-MBFITS', scan)
                i.currentFebeParMbfits.HduNum = hdu
                febeTab = i.currentFebeParMbfits  # needed as Parent of other tables
            elif name == 'ARRAYDATA-MBFITS':
                i.currentArrayDataTab = Table('ARRAYDATA-MBFITS', febeTab)
                i.currentArrayDataTab.HduNum = hdu
            elif name == 'DATAPAR-MBFITS':
                i.currentDataParTab = Table('DATAPAR-MBFITS', febeTab)
                i.currentDataParTab.HduNum = hdu
            elif name == 'MONITOR-MBFITS':
                i.currentMonitorTab = Table('MONITOR-MBFITS', scan)
                i.currentMonitorTab.HduNum = hdu

        numRows = int(i.currentArrayDataTab.Header.readKeyword('NAXIS2'))
        firstRow = 1
        lastRow = numRows
        while firstRow <= lastRow:
            prevFirstRow = firstRow
            firstRow = i.computeCoord(firstRow, lastRow)
            print 'Interpolation done up to row %g / %g\n' % (firstRow, lastRow)
            # if firstRow == prevFirstRow:
            firstRow += 1

    f.close()


def fillOffsets(fileName, listObs=[1], offsets=None, vel=None):
    """
    DES: fill LONGOFF and LATOFF columns in DATAPAR tables, computed
         from subscan lengths and velocities
    INP: fileName (str) name of input file
         listObs (list of int) numbers of observations to write
         offsets (list of tuples) scan length in (Az,El), per observation
         vel (list of tuples) drift speeds in Az and El, per observation
    """

    f = FitsFile(fileName)
    f.open(1)

    # First, read the SCAN-MBFITS table (same for all obs.)
    scan = Table('SCAN-MBFITS', f)
    scan.HduNum = 2
    # extract SCANNUM and FEBE from its content
    scannum = int(scan.Header.readKeyword('SCANNUM'))
    febe = scan.BinTable.readTableColumn('FEBE')[0]

    # now loop over the observations to find the corresponding
    # tables and run the interpolator
    numObs = 0
    for obs in listObs:
        tables = f.getObservation(scannum, obs, febe, 1)
        # last parameter (1) = FeBe section
        tables = tables[1:]  # remove SCAN table
        for tabInfo in tables:
            name = tabInfo[1]
            hdu = tabInfo[0]
            if name == 'DATAPAR-MBFITS':
                datapar = Table('DATAPAR-MBFITS', scan)
                datapar.HduNum = hdu

        integ = datapar.BinTable.readTableColumn('INTEGTIM')
        lst = datapar.BinTable.readTableColumn('LST')
        nRowsArraydata = len(integ)
        lenAz = offsets[numObs][0]
        lenEl = offsets[numObs][1]
        az_off = [(lenAz / 2. + vel[numObs][0] * integ[0] / 2.) / 3600.]
                   # at mid integration
        el_off = [(lenEl / 2. + vel[numObs][1] * integ[0] / 2.) / 3600.]
        for i in range(1, nRowsArraydata):
            az_off.append(az_off[0] + vel[numObs][
                          0] * (lst[i] - lst[0] + integ[i] / 2.) / 3600.)
            el_off.append(el_off[0] + vel[numObs][
                          1] * (lst[i] - lst[0] + integ[i] / 2.) / 3600.)
        datapar.BinTable.updateTableColumn('LONGOFF', az_off, 1)
        datapar.BinTable.updateTableColumn('LATOFF', el_off, 1)
        numObs += 1
    f.close()


def doSimba(inName="APEX-100003-T-00.0-0002-2004.fits",
            outName="APEX-1003-02-2004.fits",
            listObs=[1], timeFile=None,
            offsets=None, vel=None, source=None):

    config.DEBUG = 0

    if timeFile:
        try:
            tt = file(timeFile, 'w')
            t0 = time()
        except IOError:
            print "Can't open file for time marks"
            timeFile = None

    # keepFirst(inName,outName)
    keepObs(inName, outName, listObs, 0, 0, source)
    if timeFile:
        t1 = time()
        tt.write("keepFirst done in: %f\n" % (t1 - t0))
        t0 = t1

    for obs in listObs:
        dapNum = 3 * obs + 2  # should use getTable
        fillLst(outName, dapNum)
    if timeFile:
        t1 = time()
        tt.write("fillLst done in: %f\n" % (t1 - t0))
        t0 = t1

    # runInterpol(outName,listObs)
    if timeFile:
        t1 = time()
        tt.write("runInterpol done in: %f\n" % (t1 - t0))
        t0 = t1

    # this must be done after interpolator, which fills offsets
    # columns with zeros when unable to compute them
    fillOffsets(outName, listObs, offsets, vel)
    if timeFile:
        t1 = time()
        tt.write("fillOffsets done in: %f\n" % (t1 - t0))
        tt.close()


def examples():
    # examples of use of doSimba
    off = [(700., 0.), (0., 700.)]
    vel = [(-6., 0.), (0., -6.)]
    doSimba(
        "APEX-388-T-00.0-0002-2004.fits",
        "APEX-388.fits",
     [1,
      2],
     "t388",
     off,
     vel,
     'JUPITER')

    off = [(840., 0.), (0., 840.)]
    vel = [(-6., 0.), (0., -6.)]
    doSimba(
        "APEX-395-T-00.0-0002-2004.fits",
        "APEX-395.fits",
     [1,
      2],
     "t395",
     off,
     vel,
     'Q1226')
