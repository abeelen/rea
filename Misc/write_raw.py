# Automatically adapted for numpy.oldnumeric Nov 22, 2012 by

from mbfits import *
import numpy.oldnumeric.random_array as RandomArray
from Numeric import *

nBolo = 60
smpRate = 1e3  # 1kHz
timeSubScan = 10 * 60  # seconde
nDump = timeSubScan * int(smpRate)
nSlice = 100
mjd0 = 0.

f = FitsFile('!myTestRaw.fits', 'RAW')
f.create()

primary = Table('Primary', Parent=f)
# primary.HduNum = 1

primary.Header.updateKeyword('CREATOR', '', 'Raw FITS data writer v. 0.1')
primary.Header.updateKeyword('DATE-OBS', '', '2005-09-27T20:41:21')
primary.Header.updateKeyword('DATE', '', '2005-09-27T20:41:21')
primary.Header.updateKeyword('FRONTEND', '', 'APEXSZ')
primary.Header.updateKeyword('BACKEND', '', 'SZBE')
primary.Header.updateKeyword('FREQUENC', '', 150000000000.)
primary.Header.updateKeyword('SCAN', '', 1)
primary.Header.updateKeyword('NSUBSCAN', '', 1)
primary.Header.updateKeyword('SMPLRATE', '', smpRate)

data = Table('RAWDATA', primary)
data.create()
data.Header.updateKeyword('DATE-OBS', '', '2005-09-27T20:41:21')
data.Header.updateKeyword('OBSNUM', '', 1)
data.Header.updateKeyword('NUSEFEED', '', nBolo)
data.Header.updateKeyword('NRECAC', '', 0)
data.Header.updateKeyword('NRECDC', '', nBolo)
data.Header.updateKeyword('RECCNTRL', '', 1)
data.Header.updateKeyword('NRECORD', '', nDump)

data.BinTable.updateDimension('DATA')

# a = (RandomArray.uniform(-50.,200.,(nDump,nBolo))).astype(Int16).tolist()
# mjd = (mjd0+arange(nDump,typecode=Float)/smpRate).tolist()

sizeSlice = nDump / nSlice

for index in xrange(nSlice):

    a = (RandomArray.uniform(-50., 200., (sizeSlice, nBolo))
         ).astype(Int16).tolist()
    mjd = (mjd0 + arange(sizeSlice, typecode=Float) / smpRate).tolist()

    data.BinTable.addTableRows([mjd, a])

    mjd0 = mjd[-1] + 1. / smpRate
    del(a)
    del(mjd)


f.close()
