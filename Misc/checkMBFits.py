import os
import mbfits
import BoaConfig
import BoaDir

BoaDir.setInDir('../APEX-fits/')

listing = os.listdir(BoaConfig.inDir)
fitsfile = [filename for filename in listing if filename[-5:] == ".fits"]
for filename in fitsfile:
    try:
        BoaDir.checkFits(filename)
        print filename + " Ok"
    except mbfits.MBFITSError as msg:
        print filename + " Bad :" + str(msg)
