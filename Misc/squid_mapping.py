"""
Use :
execfile('Misc/squid_mapping.py')
SquidMap = readFeedMap ('APEXSZ_feedmap.dat')
card = 'e9:'

#chanList = getChanFromSquid(card,SquidMap)
#chanList = data.BolometerArray.checkChanList(chanList)
#data.plotCorrel(chanList[0],chanList)
#indexes = data.BolometerArray.getChannelIndexes(chanList)
#subCor = compress2d(data.CorMatrix,indexes)

#all of them sorted

cards=['e7:a','e7:b','e8:a','e8:b','e9:a','e9:b','ea:a','ea:b']
completeChanList=[]
for card in cards:
   chanList = getChanFromSquid(card,SquidMap)
   completeChanList.extend(chanList)

data.plotCorMatrix(completeChanList,check=0,style='idl4')

for i in range(len(cards)):
   plot(i*array([8,8])-1,[0,54],style='l',overplot=1)
   plot([0,54],i*array([8,8])-1,style='l',overplot=1)

for i in range(4):
   plot(i*array([16,16])-0.95,[0,54],style='l',overplot=1)
   plot([0,54],i*array([16,16])-0.95,style='l',overplot=1)
   plot(i*array([16,16])-1.05,[0,54],style='l',overplot=1)
   plot([0,54],i*array([16,16])-1.05,style='l',overplot=1)

--------------------------------------------------------------------------

# sorting one dimension with distance


# retrieve the ChanSep of the Used Channels
data.computeCorMatrix()


# sort with Cards along one dimension
cards=['e7:a','e7:b','e8:a','e8:b','e9:a','e9:b','ea:a','ea:b']
completeChanList=[]
for card in cards:
   chanList = getChanFromSquid(card,SquidMap)
   completeChanList.extend(chanList)

data.plotCorMatrix(completeChanList,check=0, distance=1, \
xLabel='channels sorted by cards: e7a e7b e8a e8b e9a e9b eaa',\
style='idl4')

#ChannelIndex = CorDistSorted.astype(Int) * 0
#for i in range(data.BolometerArray.NUsedChannels):
#   indexArray = arange(data.BolometerArray.NUsedChannels)
#   sortedIndex = take(indexArray,(argsort(ChanSep[i,:])))
#   ChannelIndex[i,:]  = sortedIndex
#   CorDistSorted[i,:] = take(CorDistSorted[i,:],(sortedIndex))

#draw(CorDistSorted,wedge=1,style='idl4', \
# labelX='channels sorted by cards: e7a e7b e8a e8b e9a e9b eaa',\
# labelY='channels in increasing distance',\
# caption='scan 405 correlation matrix after CN subtraction')

for i in range(len(cards)):
   plot(i*array([8,8])-1,[0,55],style='l',overplot=1)

for i in range(4):
   plot(i*array([16,16])-0.95,[0,54],style='l',overplot=1)
   plot(i*array([16,16])-1.05,[0,54],style='l',overplot=1)

"""

from .Utilities import compress2d


def readFeedMap(filename):
    SquidMap = [''] * data.BolometerArray.NChannels

    try:
        f = file(BoaConfig.rcpPath + filename)
    except IOError:
        print "could not open file %s" % (filename)
        return

    # Read the file and put the values in 1 list and 2 arrays

    asciiFile = f.readlines()
    f.close()
    nOffsets = len(asciiFile)

    APECS = []
    Bolo = []
    Readout = []

    for i in range(0, nOffsets):
        tmp = asciiFile[i].split()
        APECS.append(int(tmp[0]))
        Bolo.append(str(tmp[1]))
        Readout.append(str(tmp[2]))

    # Replace the SquiMap
    for i in range(len(APECS)):
        SquidMap[APECS[i] - 1] = Readout[i]

    return SquidMap


def getChanFromSquid(squid, SquidMap):

    ChanInSquid = []
    for i in range(len(SquidMap)):
        if SquidMap[i][:len(squid)] == squid:
            ChanInSquid.append(i + 1)

    return ChanInSquid
