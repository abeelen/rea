"""
NAM: split_MB.py (file)
DES: script to convert old data files (stored in one big MB-Fits file) to
     the "new" scheme with a main GROUPING.fits file and one file per table
"""

from mbfits import *
import os


def split(input, output):
    """
    NAM: split (method)
    DES: converts single-file MB-Fits to splitted files
    INP: (str) input  = input file name
         (str) output = output file name
    """

    # First, read everything from the input file
    f = FitsFile(input)
    try:
        in_ptr = f.open()
    except MBFITSError:
        print "Could not open input file: " + input
        return
    print "Reading input file: " + input
    f.read()
    f.close()
    nb = len(f.TableList)

    # Create the output file
    try:
        os.mkdir(output)
        out = FitsFile(output)
        out_ptr = out.create()
    except MBFITSError:
        print "MBFits error - could not create output file: " + output
        return
    except OSError as msg:
        print "OSError: ", msg
        print "Could not create output file: " + output
        return

    # Create first table: Primary header
    primary = Table('Primary', out)
    primary.HduNum = 1
    # Copy input Primary header
    copyKey(f.TableList[0], primary)

    # SCAN-MBFITS table: the file name is output/SCAN.fits
    outScan = Table('SCAN-MBFITS', out)
    outScan.create(output + "/SCAN.fits")

    # Now we will need the FEBE name and number of subscans
    # Get them while copying the SCAN-MBFITS content
    for i in range(nb):
        if f.TableList[i].Name == 'SCAN-MBFITS':
            inScan = f.TableList[i]
    febe, obsnum = copyScan(inScan, outScan)
    febe = febe[0]

    print "febe = ", febe
    print "nb. obs = ", obsnum

    # Now create FEBEPAR Table - assume only one FEBE (!)
    outFebe = Table('FEBEPAR-MBFITS', outScan)
    outFebe.create(filename=output + "/" + febe + "-FEBEPAR.fits", FEBE=febe)
    for i in range(nb):
        if f.TableList[i].Name == 'FEBEPAR-MBFITS':
            inFebe = f.TableList[i]
    copyFebe(inFebe, outFebe)

    # Now loop through the remaining tables
    for tab in f.TableList:
        if tab.Name == 'ARRAYDATA-MBFITS':
            subNum = tab.Header.KeywordDict['OBSNUM'][0]
            if not os.path.isdir(output + '/' + str(subNum)):
                os.mkdir(output + '/' + str(subNum))
            newTab = Table('ARRAYDATA-MBFITS', outFebe)
            # File name: the 1 at the end stands for Baseband number
            newTab.create(
                filename=output + '/' +
                    str(subNum) + "/" + febe + "-ARRAYDATA-1.fits",
                          FEBE=febe, SUBSNUM=subNum)
            copyData(tab, newTab)
        elif tab.Name == 'DATAPAR-MBFITS':
            subNum = tab.Header.KeywordDict['OBSNUM'][0]
            if not os.path.isdir(output + '/' + str(subNum)):
                os.mkdir(output + '/' + str(subNum))
            newTab = Table('DATAPAR-MBFITS', outFebe)
            newTab.create(
                filename=output + '/' +
                    str(subNum) + "/" + febe + "-DATAPAR.fits",
                          FEBE=febe, SUBSNUM=subNum)
            copyPara(tab, newTab)
        elif tab.Name == 'MONITOR-MBFITS':
            subNum = tab.Header.KeywordDict['OBSNUM'][0]
            if not os.path.isdir(output + '/' + str(subNum)):
                os.mkdir(output + '/' + str(subNum))
            newTab = Table('MONITOR-MBFITS', outFebe)
            newTab.create(
                filename=output + '/' + str(subNum) + "/MONITOR.fits",
                          FEBE=febe, SUBSNUM=subNum)
            copyMonitor(tab, newTab)

        else:
            print "Unknown table: ", tab.Name

    out.close()


def copyKey(inTab, outTab):
    """
    NAM: copyKey (method)
    DES: copy all keywords from input table's header to output table
    INP: inTab  (mbfits.Table object) = input Table
         outTab (mbfits.Table object) = output Table
    """
    kk = inTab.Header.Keywords
    for k in kk:
        outTab.Header.updateKeyword(
    k,
     '',
     log2int(inTab.Header.KeywordDict[k][0]))


def copyScan(input, output):
    copyKey(input, output)
    febe = input.BinTable.Data['FEBE']
    output.BinTable.updateTableColumn('FEBE', febe, 1)
    obsnum = input.Header.KeywordDict['NOBS'][0]
    return febe, obsnum


def copyData(input, output):
    copyKey(input, output)
    output.BinTable.updateDimension('DATA')
    mjd = input.BinTable.Data['MJD']
    data = input.BinTable.Data['DATA']
    for i in range(len(mjd)):
        output.BinTable.addTableRow([mjd[i], data[i,:,:]])


def copyFebe(input, output):
    copyKey(input, output)
    # Now the column sizes have to be adjusted
    output.BinTable.updateDimension('FEEDOFFX')
    output.BinTable.updateDimension('FEEDOFFY')
    output.BinTable.updateDimension('POLTY')
    output.BinTable.updateDimension('POLA')
    output.BinTable.updateDimension('APEREFF')
    output.BinTable.updateDimension('BEAMEFF')
    output.BinTable.updateDimension('ETAFSS')
    output.BinTable.updateDimension('HPBW')
    output.BinTable.updateDimension('ANTGAIN')
    output.BinTable.updateDimension('TCAL')
    output.BinTable.updateDimension('BOLDCOFF')
    output.BinTable.updateDimension('FLATFIEL')
    output.BinTable.updateDimension('GAINIMAG')
    # 2D arrays (feeds x band) since v 1.55
    nbUsedFeed = len(input.BinTable.Data['USEFEED'][0][0])
    output.BinTable.setVariableLengthDimension('USEFEED', nbUsedFeed, 1)
    output.BinTable.setVariableLengthDimension('BESECTS', nbUsedFeed, 1)
    output.BinTable.setVariableLengthDimension('FEEDTYPE', nbUsedFeed, 1)
    # Now copy binary table content
    data = input.BinTable.Data
    oneRow = [data['USEBAND'][0], data['NUSEFEED'][0], data['USEFEED'][0][0],
              data['BESECTS'][0][0], data[
                  'FEEDTYPE'][0][0], data['FEEDOFFX'][0],
              data['FEEDOFFY'][0], data['REFFEED'], data['POLTY'][0], data['POLA'][0]]
    oneRow.extend([data['APEREFF'][0], data['BEAMEFF'][0], data['ETAFSS'][0],
                   data['HPBW'][0], data['ANTGAIN'][0], data['TCAL'][0],
                   data['BOLCALFC'][0], data['BOLREFGN'][0], data['BOLDCOFF'][0]])
    oneRow.extend(
        [data['FLATFIEL'][0], data['GAINIMAG'][0], data['GAINELE1'][0],
                   data['GAINELE2'][0]])
    output.BinTable.addTableRow(oneRow)


def copyPara(input, output):
    copyKey(input, output)
    mjd = input.BinTable.Data['MJD']
    for numInteg in range(len(mjd)):
        oneRow = [mjd[numInteg]]
        oneRow.extend([input.BinTable.Data['LST'][numInteg]])
        oneRow.extend([input.BinTable.Data['INTEGTIM'][numInteg]])
        oneRow.extend([input.BinTable.Data['PHASE'][numInteg]])
        oneRow.extend([input.BinTable.Data['LONGOFF'][numInteg]])
        oneRow.extend([input.BinTable.Data['LATOFF'][numInteg]])
        oneRow.extend([input.BinTable.Data['AZIMUTH'][numInteg]])
        oneRow.extend([input.BinTable.Data['ELEVATIO'][numInteg]])
        oneRow.extend([input.BinTable.Data['CBASLONG'][numInteg]])
        oneRow.extend([input.BinTable.Data['CBASLAT'][numInteg]])
        oneRow.extend([input.BinTable.Data['BASLONG'][numInteg]])
        oneRow.extend([input.BinTable.Data['BASLAT'][numInteg]])
        oneRow.extend([input.BinTable.Data['ROTANGLE'][numInteg]])
        oneRow.extend([input.BinTable.Data['MCRVAL1'][numInteg]])
        oneRow.extend([input.BinTable.Data['MCRVAL2'][numInteg]])
        oneRow.extend([input.BinTable.Data['MLONPOLE'][numInteg]])
        oneRow.extend([input.BinTable.Data['MLATPOLE'][numInteg]])
        oneRow.extend([input.BinTable.Data['DFOCUS_X'][numInteg]])
        oneRow.extend([input.BinTable.Data['DFOCUS_Y'][numInteg]])
        oneRow.extend([input.BinTable.Data['DFOCUS_Z'][numInteg]])
        oneRow.extend([input.BinTable.Data['DPHI_X'][numInteg]])
        oneRow.extend([input.BinTable.Data['DPHI_Y'][numInteg]])
        oneRow.extend([input.BinTable.Data['DPHI_Z'][numInteg]])
        output.BinTable.addTableRow(oneRow)


def copyMonitor(input, output):
    # copy both header and binary table without any change
    copyKey(input, output)
    # Get length of MONVALUE and MONUNITS pointers from input header
    lenValue = input.Header.KeywordDict['TFORM3'][0]
    lenValue = string.split(lenValue, '(')[1]
    lenValue = int(lenValue[:-1])
    lenUnits = input.Header.KeywordDict['TFORM4'][0]
    lenUnits = string.split(lenUnits, '(')[1]
    lenUnits = int(lenUnits[:-1])
    output.BinTable.setVariableLengthDimension('MONVALUE', lenValue)
    output.BinTable.setVariableLengthDimension('MONUNITS', lenUnits)
    # Now copy data row by row
    nbInteg = len(input.BinTable.Data['MJD'])
    for i in range(nbInteg):
        oneRow = [input.BinTable.Data['MJD'][i]]
        oneRow.extend([input.BinTable.Data['MONPOINT'][i]])
        oneRow.extend([input.BinTable.Data['MONVALUE'][i]])
        oneRow.extend([input.BinTable.Data['MONUNITS'][i]])
        output.BinTable.addTableRow(oneRow)


def log2int(val):
    """
    DES: Utility to convert logical values from string ('F' or 'T')
         to integer (0 or 1), as required by updateKeyword.
    """

    if (val == 'F'):
        val = 0
    elif (val == 'T'):
        val = 1
    return val
