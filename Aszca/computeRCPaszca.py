#
#
# computeRCPaszca.py
#
# script to determine array parameters (RCP) from a beam map
#
# FS - 2007/3/07
#
#
import os
import ppgplot


def iterat(
    chans=[], scanNum=0, oversamp=1, sizeX=[-500, 500], sizeY=[-500, 500],
           data=None, flux=1., radius=0.):
    """
    This function solves for pointing (i.e. fits a 2D Gaussian) on each individual
    channel map, and stores the results in arrayParamOffsets, as well as in a text
    file (offsets, widths, angle, integrated and peak fluxes)
    """
    arrayParamOffsets = []

    if not os.getenv('BOA_HOME_RCP'):
        raise 'Environment variable BOA_HOME_RCP undefined'
    rcpDir = os.getenv('BOA_HOME_RCP')
    filename = os.path.join(rcpDir, "beams_%i_%s.txt" % (oversamp, scanNum))

    output = file(filename, 'w')
    yesno = '1'
    for c in chans:
        print "Channel = ", c
        num = data.BolometerArray.getChanIndex(c)[0]
        ppgplot.pgpage()
        # data.BolometerArray.RefChannel = c
        data.doMap(c, noPlot=1, oversamp=oversamp, sizeX=sizeX, sizeY=sizeY)
        # print "... NEXT TRY FIT ON MAP..Yes or NO? [1,0]"
        # yesno = raw_input()
        if yesno:
            try:
                data.solvePointingOnMap(radius=radius, plot=1)
            except:
                output.write("%3i no fit found\n" % (c))
        Plot.xyout(900, 900, str(c))
        result = data.PointingResult
        # print "... NEXT.CHANNEL ...PRESS <Enter>"; raw_input()
        if (result == -1) or (not yesno):
            output.write("%3i no fit found\n" % (c))
        else:
            offX = result['gauss_x_offset']['value']
            offY = result['gauss_y_offset']['value']
            widX = result['gauss_x_fwhm']['value']
            widY = result['gauss_y_fwhm']['value']
            ang = result['gauss_tilt']['value']
            # integ= result['gauss_int']['value']
            peak = data.PointingResult['gauss_peak']['value']
            integ = peak * widX * widY
            # relative gain (we will have to divide by g for flat-fielding)
            g = peak / flux
            result['gain'] = g
            offX = data.BolometerArray.Offsets[0, num] - offX
            offY = data.BolometerArray.Offsets[1, num] - offY

            # biasamp = data.ScanParam.BiasAmplitude[0,num]
            # biaspot = data.ScanParam.BiasPotsetting[0,num]
            # print biaspot

            text = "%3i %10.3f %10.3f %5.2f %5.2f %5.2f %f %f\n" % \
                   (c, offX, offY, widX, widY, ang, integ, peak)

            arrayParamOffsets.append({'channel': c,
                                      'result': result})
            output.write(text)
            data.BolometerArray.Offsets[0, num] = offX
            data.BolometerArray.Offsets[1, num] = offY

    data.arrayParamOffsets = arrayParamOffsets   # store results in current object

#


def plotRCP(
    chans,
     scanNum,
     num=1,
     sizeX=[800,
            -800],
     sizeY=[-700,
            700],
     data=None):
    """
    Plot ellipses showing the beams shapes, using the fits results stored
    in data.arrayParamOffsets
    """
    cap = "ASZCa Beam map - Scan %s" % (scanNum)
    plot([0], [0], limitsX=sizeX, limitsY=sizeY, nodata=1, aspect=1,
         labelX='Az offset ["]', labelY='El offset ["]', caption=cap)

    nb = len(chans)
    chanList = []
    for i in range(len(data.arrayParamOffsets)):
        chanList.append(data.arrayParamOffsets[i]['channel'])

    for i in range(nb):
        num = chanList.index(chans[i])
        theParam = data.arrayParamOffsets[num]
        x = data.BolometerArray.Offsets[
            0, chans[i] - 1]  # get x y from Offsets
        y = data.BolometerArray.Offsets[1, chans[i] - 1]
        wx = theParam['result']['gauss_x_fwhm'][
            'value']  # get shape from arrayParamOffsets
        wy = theParam['result']['gauss_y_fwhm']['value']
        tilt = theParam['result']['gauss_tilt']['value']
        Forms.ellipse(x, y, wx, wy, tilt, overplot=1)
        print i, num, chans[i], x, y
    if num:
        BogliConfig.xyouttext['color'] = 2
        for i in range(nb):
            x = data.BolometerArray.Offsets[0, chans[i] - 1]
            y = data.BolometerArray.Offsets[1, chans[i] - 1]
            Plot.xyout(x, y, str(chans[i]))
    # DeviceHandler.closeDev()

#


def plotRCP1(
    chans,
     scanNum,
     num=1,
     sizeX=[800,
            -800],
     sizeY=[-700,
            700],
     angle=0):
    """
    Plot ellipses showing the beams shapes, using the fits results stored
    in data.arrayParamOffsets
    """
    cap = "ASZCa Beam map - Scan %s angle= %5.1f" % (scanNum, angle)
    plot([0], [0], limitsX=sizeX, limitsY=sizeY, nodata=1, aspect=1,
         labelX='Az offset ["]', labelY='El offset ["]', caption=cap)

    nb = len(chans)
    chanList = []
    # for i in range(len(data.arrayParamOffsets)):
    #    chanList.append(data.arrayParamOffsets[i]['channel'])

    angle = angle * pi / 180.
    rotMatrix = array(
        [[cos(angle), -1. * sin(angle)], [sin(angle), cos(angle)]], 'f')

    for i in range(nb):
        # num = chanList.index(chans[i])
        # theParam = data.arrayParamOffsets[num]
        x = data.BolometerArray.Offsets[
            0, chans[i] - 1]  # get x y from Offsets
        y = data.BolometerArray.Offsets[1, chans[i] - 1]
        wx = data.BolometerArray.FWHM[0, chans[i] - 1]
        wy = data.BolometerArray.FWHM[1, chans[i] - 1]
        tilt = data.BolometerArray.Tilt[chans[i] - 1] / 180. * 3.1415
        if (wx + wy) != 0.:
            if wx > 33. and wy > 33. and wx < 150. and wy < 150.:
                Forms.ellipse(x, y, wx, wy, tilt, overplot=1)
                if num:
                    BogliConfig.xyouttext['color'] = 2
                    Plot.xyout(x, y, str(chans[i]))
        else:
            x, y = dot(rotMatrix, [x, y])
            Forms.ellipse(x, y, 60, 60, 0, overplot=1, ci=3)
            if num:
                BogliConfig.xyouttext['color'] = 3
                Plot.xyout(x, y, str(chans[i]))

#


def plotRCP2(chans, scanNum, num=1, sizeX=[800, -800], sizeY=[-700, 700]):
    """
    Plot ellipses showing the beams shapes, using the fits results stored
    in data.arrayParamOffsets
    """
    cap = "ASZCa Beam map - Scan %s" % (scanNum)
    plot([0], [0], limitsX=sizeX, limitsY=sizeY, nodata=1, aspect=1,
         labelX='Az offset ["]', labelY='El offset ["]', caption=cap)

    nb = len(chans)
    chanList = []
    # for i in range(len(data.arrayParamOffsets)):
    #    chanList.append(data.arrayParamOffsets[i]['channel'])

    for i in range(nb):
        # num = chanList.index(chans[i])
        # theParam = data.arrayParamOffsets[num]
        x = data.BolometerArray.Offsets[
            0, chans[i] - 1]  # get x y from Offsets
        y = data.BolometerArray.Offsets[1, chans[i] - 1]
        wx = data.BolometerArray.FWHM[0, chans[i] - 1]
        wy = data.BolometerArray.FWHM[1, chans[i] - 1]
        tilt = data.BolometerArray.Tilt[chans[i] - 1] / 180. * 3.1415
        if (wx + wy) != 0.:
            if wx > 33. and wy > 33. and wx < 150. and wy < 150.:
                Forms.ellipse(x, y, wx, wy, tilt, overplot=1)
                if num:
                    BogliConfig.xyouttext['color'] = 2
                    Plot.xyout(x, y, str(chans[i]))
