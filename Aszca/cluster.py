# small maps: 5383-5405  17M
# big maps:   5698-5703  255M

macClean('5384', rmsClip=3, plot=0, despike=1)
signal()
print data.ScanParam.SubscanNum

data.correlatedNoiseFFCF(skynoise=0, minSlope=0.1, maxSlope=10.0, plot=0, chanRef=17)

#--------------- Optional:

BogliConfig.point['size'] = 3
Plot.plot(data.Slopes[0,:], labelX='good channels', labelY='slope', limitsY=[0, 2])
BogliConfig.point['size'] = 0.01

mask = nonzero(less(data.Slopes[0,:], 0.5) or greater(data.Slopes[0,:], 3))
chanList = data.BolometerArray.checkChanList([])
for i in mask:
    flagCh(chanList[i])

#---------------    

data.computeWeights(minCorr=0., a=0.95, b=1.0, core=10., beta=2.)
data.plotCorMatrix(distance=1, weights=1)
data.computeCorrelatedNoise(clip=5.)

#--------------- Optional:

BogliConfig.point['size'] = 3
Plot.plot(data.Slopes[0,:], labelX='good channels', labelY='slope', limitsY=[0, 2])
BogliConfig.point['size'] = 0.01

#--------------- Iterate this step:

data.correlatedNoiseFFCF(skynoise=1, minSlope=0.1, maxSlope=10.0, plot=1)
data.FFCF_CN[260, 260]
data.FFCF_CN[242, 242]
data.FFCF_CN[16, 16]
data.computeWeights(minCorr=0., a=0.95, b=2.0, core=10., beta=2.)
data.computeCorrelatedNoise(clip=5.)

#---------------- Plot correlated noise

chanList = data.BolometerArray.checkChanList([])
chanListIndexes = data.BolometerArray.getChanIndex(chanList)
for iChan in chanListIndexes:    # symmetric FFCF are *Gains* after correlation with SN !!
    data.CorrelatedNoise[:, iChan] = (data.CorrelatedNoise[:, iChan]*data.FFCF_CN[iChan, iChan]).astype(Float32)

signal(noise=0)
signal(noise=1, overplot=1, ci=2)
# signal(noise=0,overplot=1,ci=3)

#-------------- subtract correlated noise

signal(range(17, 31), noise=0)
signal(range(17, 31), noise=1, overplot=1, ci=2)

for iChan in chanListIndexes:
    data.Data[:, iChan] = (data.Data[:, iChan]-data.CorrelatedNoise[:, iChan]).astype(Float32)

signal(range(17, 31), noise=0, overplot=1, ci=3)

#---------------

stat()
plotRmsChan()
flagRms(above=10)
signal()
base(order=5, subscan=0)
despike()
signal()

#---------------

base(order=3, subscan=1)

stat()
computeWeight()
mapping(sizeX=[-1100, 1100], limitsZ=[-1, 2], style='idl4', oversamp=3)

map5384 = copy.deepcopy(data.Map)

data.Map.smoothBy(60)
data.Map.display(limitsX=[1100, -1100], limitsZ=[-1, 2], style='idl4')

#----------------

from boa.Utilities import compress2d
matrix = compress2d(data.FFCF_CN, data.BolometerArray.getChanIndex(chanList))
Plot.draw(matrix, wedge=1, limitsZ=[0, 2], nan=0, style='idl4')

plotCorrel(chanList=range(18, 32), chanRef=17)


mapping(limitsZ=[-1, 2], style='idl4', oversamp=3, system='EQ')
data.Map.smoothBy(0.01666666666)
data.Map.display(limitsZ=[-1, 2], style='idl4', caption="Hello")


map5383.Data     = map5383.Data     + map5384.Data 
map5383.Weight   = map5383.Weight   + map5384.Weight
map5383.Coverage = map5383.Coverage + map5384.Coverage 
