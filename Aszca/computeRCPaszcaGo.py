#
#
# Saturn:  4522 (focused) 4536 (z-3) 4539 (z+3) 4634 (z-2)
# Jupiter: 4641 x-4, 4644 y+4

import os
if not os.getenv('BOA_HOME_ASZCA'):
    raise 'Environment variable BOA_HOME_ASZCA undefined'
exec(compile(open(os.path.join(os.getenv('BOA_HOME_ASZCA'), 'computeRCPaszca.py'))
     .read(), os.path.join(os.getenv('BOA_HOME_ASZCA'), 'computeRCPaszca.py'), 'exec'))
# execfile(os.path.join(os.getenv('BOA_HOME_ASZCA'), 'makeRCPaszca.py'))

scanNum = 5476
oversamp = 3
refChan = 102

read(str(scanNum), febe='BOLOSZ-SZACBE')
# read('APEX-4522-T-79.F-0010-2007', febe='BOLOSZ-SZACBE')
print 'COMPLETE READING FITS FILE'

# Use a good RCP file as starting point, if available
# updateRCP('ASZCa_feedmap_2007.rcp',scale=10)

# medianbase(subscan=0)
base(order=1, subscan=0)
plotRmsChan()

if size(data.ScanParam.SubscanNum) > 1:
    data.flagInTime('lst', below=0)

# flagRms(below=0.3)  # or whatever corresponds to dead channels
# flagC([16,143])           # the blind bolometer

# flagC(257)           # a very noisy one
# flagPos(radius=50)   # flag the source
# mediannoise(chanRef=46)
# despike()
# unflag(flag=5)

# now do a solvepoi() on an individual channel map. Let's say that the result peak is
# 0.00566, and the source (Saturn) is 2427 Jy. The next command scales all signals to
# a first reasonable value
# data.Data *= array(2427./0.00566,'f')

    # We will now fit gaussians to all individual channel maps
cc = data.BolometerArray.checkChanList([])  # list of usable channels
# cc=array([11,  12,  13, 14])
signal(cc)
print 'PLOTTED SIGNAL OF ALL CHANNELS'

    # if we do not have good offsets to start with, lets center the channel maps
    # by zeroing the offsets etc:

data.BolometerArray.Offsets = 0. * data.BolometerArray.Offsets
data.BolometerArray.Gain = 0. * data.BolometerArray.Gain + 1.
data.BolometerArray.ExtGain = 0. * data.BolometerArray.ExtGain + 1.
data.BolometerArray.FWHM = 0. * data.BolometerArray.FWHM
data.BolometerArray.Tilt = 0. * data.BolometerArray.Tilt
# extra for bias voltage
# data.BolometerArray.BiasAmp = 0.*data.BolometerArray.BiasAmp
# data.BolometerArray.BiasPot = 0.*data.BolometerArray.BiasPot

BogliConfig.xyouttext['charheight'] = 2.

op('beams_%i.ps/CPS' % (scanNum))  # output plots, for ckecking the fit results

iterat(
    chans=cc, scanNum=scanNum, oversamp=oversamp, sizeX=[-1000, 1000], sizeY=[-1000, 1000],
       data=data, flux=1., radius=0.)
print 'DONE FITTING OF ALL BEAMS'

close()

data.BolometerArray.RefChannel = refChan

# This will update the RCP in the current object, and write the results out
# data.BolometerArray.writeRCPfile("ASZCa_tmp.rcp")
# data.updateArrayParameters("ASZCa_tmp.rcp")

filename = "beams_%i_%s.txt" % (oversamp, scanNum)
data.BolometerArray.readAsciiRcp(filename=filename)
print 'READ RCP FILE'

op('beams_%i_%s.ps/CPS' % (oversamp, scanNum))
   # output plots, for ckecking the fit results
BogliConfig.xyouttext['charheight'] = 0.7

plotRCP2(cc, str(scanNum))

close()

filename = "aszca-%s.rcp" % (scanNum)
data.BolometerArray.writeRCPfile(rcpFile=filename)
# data.BolometerArray.writeRCPfile(rcpFile=filename)
print 'WROTE RCP FILE'
