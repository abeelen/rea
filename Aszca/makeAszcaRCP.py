#
#
# makeAszcaRCP.py
#
# determine array parameters (RCP) from a beam map
#
# FP - 2007/06/10
# based on the script computeRCPaszca.py written by FS
#
#
import ppgplot


def makeAszcaRCP(
    scanNum,
     resdir,
     oversamp=3,
     data=data,
     refChan=102,
     positionRCP='none',
     mapSize=[-1000,
              1000],
     AzEl=[]):
    """
    NAM: makeAszcaRCP()
    DES: Create an RCP file from a beam map.
    INP:     scanNum (int) : Scan number containing the beam map
              resdir (str) : directory where results will be written
    OPT:    oversamp (int) : oversampling factor for map creation
          mapSize (i list) : Az-El bounds of the created maps in arcsec
         positionRCP (str) : name of input rough RCP file
             refChan (int) : reference channel
    """

  # Read data
    data.read(str(scanNum), febe='BOLOSZ-SZACBE')
    if size(data.ScanParam.SubscanNum) > 1:
        data.flagInTime('lst', below=0)

  # Remove low-order baseline and create subscans
    base(order=1, subscan=0)

  # list of existing channels
    cc = data.BolometerArray.checkChanList([])
    data.ScanParam.findSubscanFB(azMax=800.)

  # if we do not have good offsets to start with, center the channel maps
    if positionRCP == 'none':
        print "Data Analysis without noise removal"
        data.BolometerArray.Offsets = 0. * data.BolometerArray.Offsets
        data.BolometerArray.Gain = 0. * data.BolometerArray.Gain + 1.
        data.BolometerArray.ExtGain = 0. * data.BolometerArray.ExtGain + 1.
        data.BolometerArray.FWHM = 0. * data.BolometerArray.Offsets
        data.BolometerArray.Tilt = 0. * data.BolometerArray.Gain
  # otherwise clean images by median noise removal
    else:
        print "Reading input RCP"
          # Read input RCP
        dead = updateRCP(positionRCP)
          # Obtain planet position (if not provided as input)
        if (len(AzEl) == 2):
            PlanetAz = AzEl[0]
            PlanetEl = AzEl[1]
        else:
            data.doMap(
                refChan,
                noPlot=1,
                oversamp=oversamp,
                sizeX=mapSize,
                sizeY=mapSize)
            data.solvePointingOnMap(radius=0.)
            PlanetAz = data.PointingResult['gauss_x_offset']['value']
            PlanetEl = data.PointingResult['gauss_y_offset']['value']

          # Flag Source
        flagC(dead)
        flagPosition(radius=120., flag=5, offset=1, Az=PlanetAz, El=PlanetEl)
          # Remove median noise
        data.medianNoiseRemoval()
          # Despike
        itDespike()
          # Unflag Source
        unflag(flag=5)
        unflagC(chanList=dead)

  # Open output plot file, for ckecking the fit results
    op(resdir + '/beams_%i.ps/CPS' % (scanNum))
    BogliConfig.xyouttext['charheight'] = 2.

  # Loop over channels
    arrayParamOffsets = []
    filename = resdir + "/beams_%i_%s.txt" % (oversamp, scanNum)
    output = file(filename, 'w')
    ChToPlot = []
    for c in cc:

        # Solve pointing
        print "Channel = ", c
        num = data.BolometerArray.getChanIndex(c)[0]
        data.doMap(
            c,
            noPlot=1,
            oversamp=oversamp,
            sizeX=mapSize,
            sizeY=mapSize)
        data.solvePointingOnMap(radius=0., display=0)
        result = data.PointingResult

        # Basic outputs
        if (result == -1):
            offX = -1
            offY = -1
            widX = -1
            widY = -1
            integ = 0
        else:
            offX = result['gauss_x_offset']['value']
            offY = result['gauss_y_offset']['value']
            widX = result['gauss_x_fwhm']['value']
            widY = result['gauss_y_fwhm']['value']
            integ = result['gauss_int']['value']

        # If median noise removal was applied, but the fit still fails
        # then try polynomial baseline removal
        ratio = widX / widY
        ChanNotDead = (not len(nonzero(array(dead) == c)))
        if ((positionRCP != 'none') and ChanNotDead and ((integ == 0.) or
           (widX < 32.) or (widX > 200.) or (widY < 32.) or (widY > 200.) or
           (abs(offX) > 60.) or (abs(offY) > 60.) or (ratio > 2.) or (ratio < 0.5))):

              # Re-introduce median noise and remove baseline
            data.Data[:, num] = data.Data[:, num] + data.Skynoise[
                :, num] * array(data.FF_Median[num], Float32)
            flagPosition(
                channel=c,
                radius=120.,
                flag=5,
                offset=1,
                Az=PlanetAz,
                El=PlanetEl)
            base(c, order=3, subscan=1)
            itDespike(chanList=c)
            unflag(channel=c, flag=5)

              # Solve pointing again
            data.doMap(
                c,
                noPlot=1,
                oversamp=oversamp,
                sizeX=mapSize,
                sizeY=mapSize)
            data.solvePointingOnMap(radius=0., display=0)
            result = data.PointingResult

        # Final outputs
        if (result == -1):
            output.write("%3i no fit found\n" % (c))
        else:
            offX = result['gauss_x_offset']['value']
            offY = result['gauss_y_offset']['value']
            widX = result['gauss_x_fwhm']['value']
            widY = result['gauss_y_fwhm']['value']
            ang = result['gauss_tilt']['value']
            peak = result['gauss_peak']['value']
            integ = result['gauss_int']['value']
              # output plot
            ppgplot.pgpage()
            data.showPointing()
            Plot.xyout(900, 900, str(c))

            offX = data.BolometerArray.Offsets[0, num] - offX
            offY = data.BolometerArray.Offsets[1, num] - offY
            BampSet = 0
            if (len(data.ScanParam.BiasAmplitude) == 1):
                if ((len(data.ScanParam.BiasAmplitude[0]) == 331) and (len(data.ScanParam.BiasPotsetting[0]) == 331)):
                    BampSet = 1
                    text = "%3i %10.3f %10.3f %5.2f %5.2f %5.2f %f %f %f %f\n" % \
                        (c, offX, offY, widX, widY, ang, integ, peak, (
                         data.ScanParam.BiasAmplitude[0])[num], (data.ScanParam.BiasPotsetting[0])[num])
            if (BampSet == 0):
                text = "%3i %10.3f %10.3f %5.2f %5.2f %5.2f %f %f %f %f\n" % \
                    (c, offX, offY, widX, widY, ang, integ, peak, 0., 0.)

            arrayParamOffsets.append({'channel': c,
                                      'result': result})
            output.write(text)
            data.BolometerArray.Offsets[0, num] = offX
            data.BolometerArray.Offsets[1, num] = offY
              # if results are OK, add channel to the plotting list
            if ((integ != 0.) and (widX > 32.) and (widX < 200.) and (widY > 32.) and (widY < 200.) and
               (abs(offX) < 850.) and (abs(offY) < 850.) and (ratio < 2.) and (ratio > 0.5)):
                ChToPlot.append(c)

    data.arrayParamOffsets = arrayParamOffsets   # store results in current object

    close()

    data.BolometerArray.RefChannel = refChan

    filename = resdir + "/beams_%i_%s.txt" % (oversamp, scanNum)
    data.BolometerArray.readAsciiRcp(filename=filename)

  # plot the the array, to ckeck the fit results
    op(resdir + '/beams_%i_%s.ps/CPS' % (oversamp, scanNum))
    BogliConfig.xyouttext['charheight'] = 0.7
    plotRCP2(cc, str(scanNum))

    close()

  # write RCPfile
    filename = resdir + "/aszca-%s.rcp" % (scanNum)
    oldrcpPATH = BoaConfig.rcpPath
    BoaConfig.rcpPath = './'
    data.BolometerArray.writeRCPfile(rcpFile=filename)
    BoaConfig.rcpPath = oldrcpPATH

#


def plotRCP2(chans, scanNum, num=1, sizeX=[800, -800], sizeY=[-700, 700]):
    """
    Plot ellipses showing the beams shapes, using the fits results stored
    in data.arrayParamOffsets
    """
    cap = "ASZCa Beam map - Scan %s" % (scanNum)
    plot([0], [0], limitsX=sizeX, limitsY=sizeY, nodata=1, aspect=1,
         labelX='Az offset ["]', labelY='El offset ["]', caption=cap)

    nb = len(chans)
    chanList = []

    for i in range(nb):
        # num = chanList.index(chans[i])
        # theParam = data.arrayParamOffsets[num]
        x = data.BolometerArray.Offsets[
            0, chans[i] - 1]  # get x y from Offsets
        y = data.BolometerArray.Offsets[1, chans[i] - 1]
        wx = data.BolometerArray.FWHM[0, chans[i] - 1]
        wy = data.BolometerArray.FWHM[1, chans[i] - 1]
        tilt = data.BolometerArray.Tilt[chans[i] - 1] / 180. * 3.1415
        if (wx + wy) != 0.:
            if wx > 33. and wy > 33. and wx < 150. and wy < 150.:
                Forms.ellipse(x, y, wx, wy, tilt, overplot=1)
                if num:
                    BogliConfig.xyouttext['color'] = 2
                    Plot.xyout(x, y, str(chans[i]))
