# 5699.py  for RXCJ1347.5
#
# big maps:   5698-5703  255M
# setInDir('/mnt/hgfs/VMshare/APEXSZ/Berkeley/')
# execfile('Test/aszca/5699.py')

# setInDir('/media/dvdram/')

macClean('5699', rmsClip=3, plot=0, despike=1, baseOrder=1)

# 141 channel(s) flagged automatically
# boa< I: 26277 samples flagged
# boa< I: 8 channel(s) flagged ([30, 142, 297, 298, 300, 302, 303, 304])
# boa< I: 2 channel(s) flagged ([40, 294])

signal()
BogliConfig.point['size'] = 3
plotRmsChan()
BogliConfig.point['size'] = 0.01
flagRms(above=60)
        #boa< I: 5 channel(s) flagged ([36, 94, 101, 231, 295])
flagC([10, 33, 34, 37, 92, 271])  # these look noisy # leaves 13x13 channels ok
stat()
despike()

data.correlatedNoiseRemoval(plot=1, chanRef=17, threshold=0.001, iterMax=2)
# start 18:54 correlation 1902 swapping, fini scan loop 1911 1925 ca1943

print "... waiting ......... PRESS <Enter> to ocntinue or CTRL-C"
raw_input()

data.dumpData('5699-sn.data')
stat()
despike()   # 22490 samples flagged, 233,234 still have strong spikes
flagRms(above=20)
        # 8 channel(s) flagged ([11, 54, 55, 233, 234, 238, 265, 266])
signal(17)
base(order=7, subscan=0)
signal(17, overplot=1, ci=2)
base(order=4, subscan=1)
despike(below=-4, above=4)  # 19557 samples flagged
signal()

from .fortran import fStat
stat()
meanRms = fStat.f_mean(data.getChanListData(type='rms'))
medianRms = fStat.f_median(data.getChanListData(type='rms'))
rmsRms = fStat.f_rms(data.getChanListData(type='rms'), meanRms)
print (meanRms, medianRms, rmsRms)
       # (5.5266389846801758, 4.702298641204834, 2.7225825786590576)
size(data.BolometerArray.checkChanList([]))  # 161


computeWeight()
mapping(sizeX=[-1100, 1100], limitsZ=[-1, 2], style='idl4', oversamp=2)
data.Map.smoothBy(50)
data.Map.display(limitsX=[1100, -1100], limitsY=[1100, -1100], limitsZ=[-1, 2],
                 style='idl4', caption=data.ScanParam.caption())

op('5699-50.ps/CPS')
close(3)

flagRms(above=10)
        # 11 channel(s) flagged ([9, 12, 24, 53, 96, 124, 127, 130, 213, 272,
        # 296])
base(order=9, subscan=1)
stat()
computeWeight()
mapping(sizeX=[-1100, 1100], limitsZ=[-1, 2], style='idl4', oversamp=3)

op('5699-9-50.ps/CPS')
close(3)
