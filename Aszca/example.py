# FB 21.5.2007
# example.py
# examples script to reduce blank field scan
#
macClean('5479', gain=0, newrcp=1)  # this is a very spiky scan

base(order=3, subscan=0)
despike()

# 227 - 261 have very low gain, 308, 310, high gain.
# look at individual signals and correlations:

signal()
signal(range(221, 262))
signal(range(308, 311))
plotCorrel(chanList=[308], chanRef=17)
plotCorrel(chanList=[308], chanRef=310)

data.correlatedNoiseFFCF(chanRef=17, plot=1, skynoise=0)
                         # will compute flat field relative to channel 17
# if you just want to subtract skynoise no need to first compute FFCF
# since that is done in the following routine
data.correlatedNoiseRemoval(plot=1, chanRef=17, threshold=0.001, iterMax=2)

# seems that first channel in correlation plot initially shows wrong slope. Need to debug!
# offaxis tracks in 179-81, 193, 199....

despike()
base(order=9, subscan=0)
despike()
data.ScanParam.findSubscanFB(azMax=2000.)
base(order=5, subscan=1)
despike()

data.getChanListData(type='rms')
data.BolometerArray.checkChanList([])

data.signalHist()
data.plotCorMatrix(distance=1)

# alternative: (only works on some computers because of bug)
eig = data.corrPCA(order=5)
