virtualenv --system-site-packages env
virtualenv env
source env/bin/activate

numpy & scipy
=============

http://www.scipy.org/

pip install numpy
pip install scipy


pyslalib
========

https://github.com/scottransom/pyslalib


mpfit
=====

https://code.google.com/p/astrolibpy/downloads/list

ppgplot
=======

https://code.google.com/p/ppgplot/

p_cfitsio
=========
Custom swig wrapper of the cfitsio library, need the libcfitsio3-dev package


