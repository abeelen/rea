"""
NAM: ExamplePointing2.py
DES: test solving pointing
USE: python
     execfile('Test/ExamplePointing2.py')
"""

#
# Example script to test solving pointing
#
#
import os
datadir = os.environ.get("BOA_HOME_EXAMPLES")
if datadir:
    indir(datadir)

print 'Example script to test solving pointing'
openDev()
print 'you may resize the window as you like, return to continue'
raw_input()
DeviceHandler.resizeDev()
proj('T-77.F-0002-2006')
print 'Example of a strong source (Jupiter)'
print 'reading in file...'
read('42947')        # Strong source (Jupiter)
signal()             # have a first look at the signal in all pixels
print 'have a first look at the signal in all pixels'
print 'return to continue, Ctrl C to stop'
raw_input()
signal(1)            # have a first look at an individual pixel
print 'have a first look at an individual pixel'
print 'return to continue, Ctrl C to stop'
raw_input()
mapping()            # make a first-look map
print 'make a first-look map'
print 'return to continue, Ctrl C to stop'
raw_input()
medianBaseline()
sig()                # see how signal looks now
print 'remove median baseline and see how signal looks now'
print 'return to continue, Ctrl C to stop'
raw_input()
mapping(oversamp=3)  # make map
print 'make a map'
print 'return to continue, Ctrl C to stop'
raw_input()
print 'solve pointing'
solvepointing(plot=1)  # solve pointing
print 'return to continue, Ctrl C to stop'
raw_input()
#
clear()
print 'Example of a fainter source (Uranus)'
print 'reading in file...'
read('46117')        # Fainter source (Uranus)
signal()             # have a first look at the signal in all pixels
print 'have a first look at the signal in all pixels'
print 'return to continue, Ctrl C to stop'
raw_input()
mapping()            # make a first-look map
print 'make a first-look map'
print 'return to continue, Ctrl C to stop'
raw_input()
medianBaseline()
sig()                # see how signal looks now
print 'remove median baseline and see how signal looks now'
print 'return to continue, Ctrl C to stop'
raw_input()
medianNoiseRemoval()
plotRmsChan()        # check at what rms most channels are
print 'remove median noise value and then see at what rms most channels are'
print 'return to continue, Ctrl C to stop'
raw_input()
flagRms(above=20)    # flag whatever rms seems too high, in this case 20
plotRmsChan()        # see the data again with flagged channels
print 'flagging channels with RMS values above a certain value'
print 'see the data again with flagged channels'
print 'return to continue, Ctrl C to stop'
raw_input()
mapping(oversamp=3)  # make map
print 'make a map'
print 'return to continue, Ctrl C to stop'
raw_input()
print 'solve pointing'  # solve pointing
solvepointing(plot=1)
print 'return to continue, Ctrl C to stop'
raw_input()
close()
print 'end of example'
