"""
NAM: ExampleMap2.py
DES: test map making process
USE: python
     execfile('Test/ExampleMap2.py')
"""

#
# Example script to make an OTF map of Orion-IRC2
#
#
import os
datadir = os.environ.get("BOA_HOME_EXAMPLES")
if datadir:
    indir(datadir)

print 'Example script to make an OTF map of Orion-IRC2'
openDev()
print 'you may resize the window as you like, return to continue'
raw_input()
DeviceHandler.resizeDev()
proj('T-77.F-0002-2006')
print 'reading in file...'
read('59491')
signal()             # have a first look at the signal in all pixels
print 'have a first look at the signal in all pixels'
print 'return to continue, Ctrl C to stop'
raw_input()
signal(1)            # have a first look at an individual pixel
print 'have a first look at an individual pixel'
print 'return to continue, Ctrl C to stop'
raw_input()
mapping()            # make a first-look map
print 'make a first-look map'
print 'return to continue, Ctrl C to stop'
raw_input()
medianBaseline()
sig()                # see how signal looks now
print 'remove median baseline and see how signal looks now'
print 'return to continue, Ctrl C to stop'
raw_input()
plotRmsChan()        # check at what rms most channels are
print 'check at what rms most channels are'
print 'return to continue, Ctrl C to stop'
raw_input()
flagRms(above=1)
flagRms(below=0.2)
plotRmsChan()        # see the data again with flagged channels
print 'flagging channels with RMS values above and below certain values'
print 'see the data again with flagged channels'
print 'return to continue, Ctrl C to stop'
raw_input()
updateRCP('jup-44830-32-improved.rcp')  # read (so far) best RCP
flagPos(radius=150.)  # Flag source position with flag value 8
mapping()
        # make another quick map to see which positions have been flagged
print 'make another quick map to see which positions have been flagged'
print 'return to continue, Ctrl C to stop'
raw_input()
base(order=1)
medianNoiseRemoval()
plotRmsChan()        # check new distribution
print 'check new distribution after removing a baseline and median noise value'
print 'return to continue, Ctrl C to stop'
raw_input()
flagRms(above=0.5)   # flag RMS values above 0.5
plotRmsChan()
print 'flagging channels with RMS values above a certain value'
print 'see the data again with flagged channels'
print 'return to continue, Ctrl C to stop'
raw_input()
sig()                # see how signal looks now
print 'see how signal looks now'
print 'return to continue, Ctrl C to stop'
raw_input()
flagC([140, 227])     # flag bad channels
despike()
sig()                # see how signal looks now
print 'flag certain bad channels and despike the data'
print 'see how signal looks now'
print 'return to continue, Ctrl C to stop'
raw_input()
print 'computing weights'
computeWeight()   # compute weights
# data._DataAna__statistics()
# weight = 1./data.ChanRms**2
# for i in range(0,data.ScanParam.NInt):
# data.DataWeights[i,:] = weight.astype(Float32)
unflag(flag=8)       # unflag source
mapping()            # make a quick map in Az-El coordinates
print 'unflag channels again'
print 'make a quick map in Az-El coordinates'
print 'return to continue, Ctrl C to stop'
raw_input()
doMap(system='EQ', sizeX=[83.9, 83.73], sizeY=[-5.48, -5.28], oversamp=5.)
      # make a nicer map in EQ coordinates
print 'make a nicer map in EQ coordinates'
print 'return to continue, Ctrl C to stop'
raw_input()
smooth(6. / 3600.)  # smooth the map
mapdisp(caption=data.ScanParam.caption())  # re-display the now smoothed map
print 'smooth the map and redisplay'
print 'return to continue, Ctrl C to stop'
raw_input()
close()
print 'end of example'
